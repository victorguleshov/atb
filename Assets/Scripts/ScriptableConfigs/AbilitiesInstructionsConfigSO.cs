using System;
using System.Collections;
using System.Collections.Generic;
using BattleSystem.Configs;
using SIDGIN.Common;
using SIDGIN.GoogleSheets;
using UnityEngine;

namespace BattleSystem.Unity.Configs
{
    [CreateAssetMenu (fileName = "AbilitiesInstructionsConfigSO", menuName = "Sheets/AbilitiesInstructionsConfigSO")]
    [GoogleSheet ("Abilities Table", "Abilities")]
    [Serializable] public class AbilitiesInstructionsConfigSO : ScriptableObject, ICollectionSet<AbilitiesInstructionsConfigSO.Row>
    {
        public AbilityInstructionsConfig Config;

        [Serializable] public class Row : IEnumerable<string>
        {
            [SerializeField, GoogleSheetParam] private List<string> row;
            public string this [int i] => row[i];

            public int Count => row.Count;
            public IEnumerator<string> GetEnumerator () => row.GetEnumerator ();
            IEnumerator IEnumerable.GetEnumerator () => GetEnumerator ();
        }

        void ICollectionSet<Row>.SetCollection (List<Row> table)
        {
            Config = new AbilityInstructionsConfig (Parse (table, 0, table.Count, 0));
        }

        List<AbilityInstructionData> Parse (IList<Row> table, int startRow, int endRow, int startCol)
        {
            var result = new List<AbilityInstructionData> ();

            var abilityID = string.Empty;
            var instructionType = string.Empty;

            for (var row = startRow; row < endRow; row++)
            {
                for (var col = startCol; col < table[row].Count; col++)
                {
                    var isSuccessfully = false;
                    if (table[row][col] == "abilityID")
                    {
                        abilityID = table[row][col + 1];
                        isSuccessfully = true;
                        row++;
                    }

                    if (table[row][col] == "instructionType")
                    {
                        instructionType = table[row][col + 1];
                        isSuccessfully = true;
                        row++;
                    }

                    if (table[row][col] == "args")
                    {
                        // var targetType = table[row][col + 1];
                        isSuccessfully = true;

                        // row++;

                        var indexOfEndArgs = row + 1;
                        while (indexOfEndArgs < endRow
                               && col < table[indexOfEndArgs].Count
                               && string.IsNullOrWhiteSpace (table[indexOfEndArgs][col])
                              )
                        {
                            indexOfEndArgs++;
                        }

                        var args = new List<NameValue> ();
                        for (var i = row; i < indexOfEndArgs; i++)
                        {
                            args.Add (new NameValue (table[i][startCol + 1], table[i][startCol + 2]));
                        }

                        var indexOfEnd = indexOfEndArgs;
                        while (indexOfEnd < endRow
                               && col < table[indexOfEnd].Count
                               && table[indexOfEnd][col] != "abilityID"
                               && (col == 0 || table[indexOfEnd][col - 1] != "if" && table[indexOfEnd][col - 1] != "else")
                              )
                        {
                            indexOfEnd++;
                        }

                        if (indexOfEndArgs == indexOfEnd)
                        {
                            result.Add (new AbilityInstructionData (abilityID, instructionType, args));
                            row = indexOfEnd - 1;
                        }
                        else
                        {
                            row = indexOfEndArgs;
                            List<NextInstructionData> nextInstructions = new List<NextInstructionData> ();
                            for (int i = row; i < indexOfEnd; i++)
                            {
                                if (row < table.Count && col < table[row].Count && table[row][col] == "if")
                                {
                                    var checkExpression = table[row][col + 1];
                                    row++;

                                    var indexOfEndIf = row + 1;
                                    while (indexOfEndIf < endRow
                                           && col < table[indexOfEndIf].Count
                                           && string.IsNullOrWhiteSpace (table[indexOfEndIf][col])
                                          )
                                    {
                                        indexOfEndIf++;
                                    }
                                    var positiveAction = Parse (table, row, indexOfEndIf, startCol + 1);

                                    row = indexOfEndIf;

                                    List<AbilityInstructionData> negativeAction = null;

                                    if (row < table.Count)
                                    {
                                        if (col < table[row].Count && table[row][col] == "else")
                                        {
                                            row++;
                                            var indexOfEndElse = row + 1;
                                            while (
                                                indexOfEndElse < endRow
                                                && col < table[indexOfEndElse].Count
                                                && string.IsNullOrWhiteSpace (table[indexOfEndElse][col])
                                            )
                                            {
                                                indexOfEndElse++;
                                            }

                                            negativeAction = Parse (table, row, indexOfEndElse, startCol + 1);

                                            row = indexOfEndElse;
                                        }
                                        else
                                        {
                                            negativeAction = new List<AbilityInstructionData> ();
                                        }
                                    }
                                    nextInstructions.Add (new NextInstructionData (checkExpression, positiveAction, negativeAction));
                                }
                            }

                            result.Add (new AbilityInstructionData (abilityID, instructionType, args, nextInstructions));
                            row = indexOfEnd - 1;
                        }
                    }

                    if (isSuccessfully)
                    {
                        break;
                    }
                }
            }
            return result;
        }
    }
}