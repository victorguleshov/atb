using System;
using UnityEngine;

namespace ATB
{
    public enum SpeedScale
    {
        Instantly = 0,
        Normal = 100,
        Fast = 150,
        Slow = 50
    }

    [Flags] public enum FriendlyFlag
    {
        Everything = -1,
        None = 0,
        Player = 1 << 0,
        Enemy = 1 << 1
    }
}