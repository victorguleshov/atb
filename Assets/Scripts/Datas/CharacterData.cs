using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace ATB.Datas
{
    [Serializable] public class StatData
    {
        [SerializeField] private string key;
        [SerializeField] private int value;

        public string Key => key;
        public int Value => value;
    }

    [Serializable] public class CharacterData : ISerializationCallbackReceiver
    {
        [SerializeField] protected string id;
        [SerializeField] protected List<StatData> stats;
        [SerializeField] protected List<string> abilities;

        public string ID => id;
        public ReadOnlyCollection<StatData> Stats { get; private set; }
        public ReadOnlyCollection<string> Abilities { get; private set; }

        public void OnBeforeSerialize () { }
        public void OnAfterDeserialize ()
        {
            Stats = stats.AsReadOnly ();
            Abilities = abilities.AsReadOnly ();
        }
    }
}