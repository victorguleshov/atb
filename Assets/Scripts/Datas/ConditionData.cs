using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ATB.Datas
{
    public enum OperatorComparison
    {
        Equality,
        Inequality,
        Less,
        LessThanOrEqual,
        Greater,
        GreaterThanOrEqual
    }

    [Serializable] public class ConditionData : ISerializationCallbackReceiver
    {
        private static readonly Dictionary<OperatorComparison, string> ComplianceDictionary =
            new Dictionary<OperatorComparison, string>
            {
                { OperatorComparison.Equality, "==" },
                { OperatorComparison.Inequality, "!=" },
                { OperatorComparison.Greater, ">" },
                { OperatorComparison.Less, "<" },
                { OperatorComparison.GreaterThanOrEqual, ">=" },
                { OperatorComparison.LessThanOrEqual, "<=" }
            };

        [SerializeField] protected string conditionType;
        [SerializeField] protected string operatorComparison;
        [SerializeField] protected string value;

        public ConditionData (string value, string key, OperatorComparison operatorComparison)
        {
            this.value = value;
            Key = key;
            OperatorComparison = operatorComparison;
        }

        public string Key { get; protected set; }
        public OperatorComparison OperatorComparison { get; protected set; }
        public string Value => value;

        public virtual void OnBeforeSerialize ()
        {
            operatorComparison = ComplianceDictionary[OperatorComparison];
            conditionType = Key;
        }
        public virtual void OnAfterDeserialize ()
        {
            foreach (var pair in ComplianceDictionary.Where (pair => pair.Value.Equals (operatorComparison)))
            {
                OperatorComparison = pair.Key;
                break;
            }

            Key = conditionType;
        }
    }
}