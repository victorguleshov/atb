using System;
using System.Collections.Generic;
using System.Linq;
using ATB.UI;
using BattleSystem.Configs;
using BattleSystem.Core;
using BattleSystem.Unity.Configs;
using Stats;
using UnityEngine;

namespace ATB
{
    public class LevelStarter : MonoBehaviour
    {
        [SerializeField] private AbilitiesInstructionsConfigSO abilityInstructions;
        [SerializeField] private SelectAbilityView selectAbilityView;
        [SerializeField] private CharacterStatsLayout characterStatsLayout;
        [SerializeField] private CharacterInitiativeLayout characterInitiativeLayout;

        private CastOrdersSystem castOrderSystem;
        private AbilityFactory abilityFactory;

        private Player_PartyController playerController;
        private Enemy_PartyController enemyController;

        private void Awake ()
        {
            var partyData = new PartyData
            {
                characters = new List<Character>
                {
                    new (
                        id: "player/char1",
                        traits: new Traits (
                            new TraitClass (attributes: new List<TraitClass.AttributeData>
                            {
                                new (
                                    BattleSystem.Configs.Constants.HP,
                                    0,
                                    BattleSystem.Configs.Constants.MAX_HP,
                                    1f
                                ),
                                new (
                                    BattleSystem.Configs.Constants.PREPARATION,
                                    0,
                                    BattleSystem.Configs.Constants.MAX_PREPARATION,
                                    0f
                                )
                            }, stats: new List<Stat>
                            {
                                new (BattleSystem.Configs.Constants.MAX_HP, 100),
                                new (BattleSystem.Configs.Constants.SPEED, 30),
                                new (BattleSystem.Configs.Constants.MAX_PREPARATION, 100),
                            })
                        ),
                        abilities: new List<AbilityData>
                        {
                            new ("slash"),
                        },
                        friendlyFlag: BattleSystem.Core.FriendlyFlag.Team0
                    )
                }
            };
            var levelData = new LevelData
            {
                characters = new List<Character>
                {
                    new (
                        id: "player/char2",
                        traits: new Traits (
                            new TraitClass (attributes: new List<TraitClass.AttributeData>
                            {
                                new (
                                    BattleSystem.Configs.Constants.HP,
                                    0,
                                    BattleSystem.Configs.Constants.MAX_HP,
                                    1f
                                ),
                                new (
                                BattleSystem.Configs.Constants.PREPARATION,
                                0,
                                BattleSystem.Configs.Constants.MAX_PREPARATION,
                                0f
                                )
                            }, stats: new List<Stat>
                            {
                                new (BattleSystem.Configs.Constants.MAX_HP, 100),
                                new (BattleSystem.Configs.Constants.SPEED, 20),
                                new (BattleSystem.Configs.Constants.MAX_PREPARATION, 100),
                            })
                        ),
                        abilities: new List<AbilityData>
                        {
                            new ("slash"),
                        },
                        friendlyFlag: BattleSystem.Core.FriendlyFlag.Team1
                    )
                }
            };

            abilityFactory = new AbilityFactory (abilityInstructions.Config);

            castOrderSystem = new CastOrdersSystem (partyData, levelData, abilityFactory);

            playerController = new Player_PartyController (partyData.characters);
            enemyController = new Enemy_PartyController (levelData.characters, castOrderSystem);

            selectAbilityView.SetController (playerController, castOrderSystem);

            characterStatsLayout.SetCastOrderSystem (castOrderSystem);
            characterInitiativeLayout.SetCastOrderSystem (castOrderSystem);
        }

        private void Update ()
        {
            castOrderSystem.MainLoop (Time.deltaTime);
        }
    }

    public class Enemy_PartyController
    {
        private CastOrdersSystem castOrderSystem;
        public Enemy_PartyController (List<Character> characters, CastOrdersSystem castOrderSystem)
        {
            foreach (var character in characters)
            {
                character.SelectSpellForCharacter += SelectSpellForCharacter;
            }

            this.castOrderSystem = castOrderSystem;
        }
        private void SelectSpellForCharacter (Character character)
        {
            var shuffledAbilities = character.Abilities.ToList ().Shuffle ();

            foreach (var abilityData in shuffledAbilities)
            {
                character.PrepareSpell (abilityData, castOrderSystem.GetRandomTargetsByType (character, abilityData.TargetType));
                return;
            }
        }
    }

    public class Player_PartyController
    {
        public Player_PartyController (List<Character> characters)
        {
            foreach (var character in characters)
            {
                character.SelectSpellForCharacter += SelectSpellForCharacter;
            }
        }

        public event Action<Character> PlayerTurn;

        private void SelectSpellForCharacter (Character character)
        {
            PlayerTurn?.Invoke (character);
        }
    }
}