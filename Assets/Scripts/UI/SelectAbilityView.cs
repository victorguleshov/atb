using System.Collections.Generic;
using BattleSystem.Core;
using UnityEngine;
using Utilities;
using Utilities.Extensions;

namespace ATB.UI
{
    public class SelectAbilityView : MonoBehaviour
    {
        [SerializeField] private List<TextAndImage> actionButtons;

        private Player_PartyController playerPartyController;
        private CastOrdersSystem castOrdersSystem;

        private void Start ()
        {
            foreach (var button in actionButtons)
                button.gameObject.SetActive (false);
        }

        private void OnEnable ()
        {
            playerPartyController.PlayerTurn += OnPlayerTurn;
        }
        private void OnDisable ()
        {
            playerPartyController.PlayerTurn -= OnPlayerTurn;
        }

        private void OnPlayerTurn (Character character)
        {
            castOrdersSystem.GameState = GameState.Cast;
            ViewExtensions.SetViews (
                actionButtons, character.Abilities, (view, abilityData, index) =>
                {
                    view.text = abilityData.ID + $" ({abilityData.TargetType.ToString ()})";
                    view.onClick.RemoveAllListeners ();
                    view.onClick.AddListener (() =>
                        {
                            character.PrepareSpell (abilityData, castOrdersSystem.GetRandomTargetsByType (character, abilityData.TargetType));

                            // character.PrepareSpell (abilityData, castOrdersSystem.CharactersOnBoard.Find (x => !character.FriendlyFlag.HasFlag (x.FriendlyFlag)));
                            castOrdersSystem.GameState = GameState.Normal;
                            foreach (var VARIABLE in actionButtons) VARIABLE.gameObject.SetActive (false);
                        }
                    );
                    return true;
                });
        }
        public void SetController (Player_PartyController playerController, CastOrdersSystem castOrdersSystem)
        {
            playerPartyController = playerController;
            this.castOrdersSystem = castOrdersSystem;
        }
    }
}