using System.Collections.Generic;
using BattleSystem.Configs;
using BattleSystem.Core;
using UnityEngine;
using Utilities;

namespace ATB
{
    public static class PointerType
    {
        public const string HERO = "hero";
        public const string ENEMY = "enemy";
    }

    public class CharacterInitiativeLayout : MonoBehaviour
    {
        // ContentSizeFitter;
        [SerializeField] private RectTransform waitBackground;
        [SerializeField] private RectTransform castBackground;

        [SerializeField] private CharacterInitiativePointer heroPointerPrefab;
        [SerializeField] private CharacterInitiativePointer enemyPointerPrefab;

        [Space]
        private CastOrdersSystem castOrdersSystem;
        [Space]
        [SerializeField] private List<TextAndImage> actionButtons;
        private readonly List<CharacterInitiativePointer> listOfPointers = new List<CharacterInitiativePointer> ();

        private Dictionary<string, ObjectPool<CharacterInitiativePointer>> pools;

        private void Awake ()
        {
            InitializeOfPools ();
        }

        private void Start ()
        {
            waitBackground.anchorMin = Vector2.zero;
            waitBackground.anchorMax = new Vector2 (castOrdersSystem.waitScaleSize, 1);
            castBackground.anchorMin = new Vector2 (castOrdersSystem.waitScaleSize, 0);
            castBackground.anchorMax = Vector2.one;

            foreach (var character in castOrdersSystem.AllCharactersList)
                listOfPointers.Add (GetView (character.ID));

            foreach (var button in actionButtons)
                button.gameObject.SetActive (false);
        }
        
        public void SetCastOrderSystem (CastOrdersSystem castOrdersSystem)
        {
            this.castOrdersSystem = castOrdersSystem;
            castOrdersSystem.UpdateCompletedEvent += RefreshViews;
        }
        private void OnDestroy ()
        {
            castOrdersSystem.UpdateCompletedEvent -= RefreshViews;
        }

        private void InitializeOfPools ()
        {
            pools = new Dictionary<string, ObjectPool<CharacterInitiativePointer>>
            {
                {
                    PointerType.HERO,
                    new ObjectPool<CharacterInitiativePointer> ()
                        .SetPrefab (heroPointerPrefab)
                        .Initialize (1)
                },
                {
                    PointerType.ENEMY,
                    new ObjectPool<CharacterInitiativePointer> ()
                        .SetPrefab (enemyPointerPrefab)
                        .Initialize (1)
                }
            };
        }

        public CharacterInitiativePointer GetView (string key)
        {
            return pools[pools.ContainsKey (key) ? key : PointerType.ENEMY].GetObject ();
        }

        private void RefreshViews ()
        {
            for (var i = 0; i < castOrdersSystem.AllCharactersList.Count; i++)
            {
                listOfPointers[i].Value = (float) castOrdersSystem.AllCharactersList[i].Traits.RuntimeAttributes.Get (Constants.PREPARATION).Ratio;
            }
        }
    }
}