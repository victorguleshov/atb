using UnityEngine;
using Utilities;

namespace ATB
{
    public class CharacterInitiativePointer : MonoBehaviour, IPooledObject<CharacterInitiativePointer>
    {
        private RectTransform rectTransform;

        private float value;

        public float Value
        {
            get => value;
            set
            {
                this.value = value;
                rectTransform.anchorMin = new Vector2 (value, rectTransform.anchorMin.y);
                rectTransform.anchorMax = new Vector2 (value, rectTransform.anchorMax.y);
            }
        }

        private void Awake ()
        {
            rectTransform = transform as RectTransform;
        }

        public ObjectPool<CharacterInitiativePointer> Pool { get; set; }
    }
}