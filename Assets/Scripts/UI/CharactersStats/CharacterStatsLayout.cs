using System;
using System.Collections.Generic;
using System.Text;
using BattleSystem.Core;
using Stats;
using UnityEngine;
using Utilities;

namespace ATB
{
    public class CharacterStatsLayout : MonoBehaviour
    {
        [SerializeField] private CharacterStatsView heroPointerPrefab;
        [SerializeField] private CharacterStatsView enemyPointerPrefab;

        [Space]
        private CastOrdersSystem castOrdersSystem;
        private readonly List<CharacterStatsView> listOfStatsViews = new List<CharacterStatsView> ();

        private bool needToRefresh;

        private Dictionary<string, ObjectPool<CharacterStatsView>> pools;

        private void Awake ()
        {
            // set target frame rate to 60
            Application.targetFrameRate = 60;
            InitializeOfPools ();
        }

        private void Start ()
        {
            foreach (var character in castOrdersSystem.AllCharactersList)
            {
                listOfStatsViews.Add (GetView (character.ID));
                character.Traits.RuntimeStats.EventChange += OnStatChanged;
                character.Traits.RuntimeAttributes.EventChange += OnAttributeChange;
            }
            needToRefresh = true;
        }
        
        public void SetCastOrderSystem (CastOrdersSystem castOrdersSystem)
        {
            this.castOrdersSystem = castOrdersSystem;
        }

        private void Update ()
        {
            if (needToRefresh) RefreshViews ();
        }
        private void OnStatChanged (IDString id)
        {
            needToRefresh = true;
        }
        private void OnAttributeChange (IDString id)
        {
            needToRefresh = true;
        }

        private void InitializeOfPools ()
        {
            pools = new Dictionary<string, ObjectPool<CharacterStatsView>>
            {
                {
                    PointerType.HERO,
                    new ObjectPool<CharacterStatsView> ()
                        .SetPrefab (heroPointerPrefab)
                        .Initialize (1)
                },
                {
                    PointerType.ENEMY,
                    new ObjectPool<CharacterStatsView> ()
                        .SetPrefab (enemyPointerPrefab)
                        .Initialize (1)
                }
            };
        }

        public CharacterStatsView GetView (string key)
        {
            return pools[pools.ContainsKey (key) ? key : PointerType.ENEMY].GetObject ();
        }

        private void RefreshViews ()
        {
            needToRefresh = false;
            for (var i = 0; i < castOrdersSystem.AllCharactersList.Count; i++)
            {
                var character = castOrdersSystem.AllCharactersList[i];

                var stringBuilder = new StringBuilder ();

                if (character.Traits.RuntimeStats.Count > 0)
                {
                    stringBuilder.Append ("--- STATS ---\n");
                    
                    foreach (var key in character.Traits.RuntimeStats.StatsKeys)
                    {
                        var stat = character.Traits.RuntimeStats.Get (key);
                        stringBuilder.Append ($"[{stat.ID}]: ");
                        if (Math.Abs (stat.Value - stat.Base) < 0.001f)
                            stringBuilder.Append ($"{stat.Value:N2}\n");
                        else if (stat.Value > stat.Base)
                            stringBuilder.Append ($"<color=\"green\">{stat.Value:N2} (+{stat.Value - stat.Base:N2})</color>\n");
                        else
                            stringBuilder.Append ($"<color=\"red\">{stat.Value:N2} (-{stat.Base - stat.Value:N2})</color>\n");
                    }
                }

                if (character.Traits.RuntimeAttributes.Count > 0)
                {
                    stringBuilder.Append ("--- VALUES ---\n");
                    foreach (var key in character.Traits.RuntimeAttributes.AttributesKeys)
                    {
                        var attribute = character.Traits.RuntimeAttributes.Get (key);
                        stringBuilder.Append ($"[{attribute.ID}]: ");
                        if (Math.Abs (attribute.Value - attribute.MaxValue) < 0.001f)
                            stringBuilder.Append ($"{attribute.Value:N2}\n");
                        else if (attribute.Value > attribute.MaxValue)
                            stringBuilder.Append ($"<color=\"green\">{attribute.Value:N2} (+{attribute.Value - attribute.MaxValue:N2})</color>\n");
                        else
                            stringBuilder.Append ($"<color=\"red\">{attribute.Value:N2} (-{attribute.MaxValue - attribute.Value:N2})</color>\n");
                    }
                }
                //
                // if (character.Traits.StatusEffects.Count > 0)
                // {
                //     stringBuilder.Append ("--- EFFECTS ---\n");
                //     foreach (var statusEffect in character.StatController.StatusEffects) stringBuilder.Append ($"<color=\"yellow\">{statusEffect.Key}</color>\n");
                // }

                listOfStatsViews[i].text.text = stringBuilder.ToString ();
            }
        }
    }
}