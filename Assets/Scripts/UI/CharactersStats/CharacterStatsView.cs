using TMPro;
using UnityEngine;
using Utilities;

namespace ATB
{
    public class CharacterStatsView : MonoBehaviour, IPooledObject<CharacterStatsView>
    {
        public TextMeshProUGUI text;

        public ObjectPool<CharacterStatsView> Pool { get; set; }
    }
}