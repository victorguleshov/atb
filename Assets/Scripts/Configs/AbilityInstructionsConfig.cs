using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace BattleSystem.Configs
{
    [Serializable]
    public class AbilityInstructionsConfig
    {
        [SerializeField]
        private List<AbilityInstructionData> abilities;

        private ReadOnlyCollection<AbilityInstructionData> abilitiesWrapper;
        public AbilityInstructionsConfig (List<AbilityInstructionData> abilities)
        {
            this.abilities = abilities;
            abilitiesWrapper = abilities.AsReadOnly ();
        }

        public ReadOnlyCollection<AbilityInstructionData> Abilities => abilitiesWrapper ??= abilities.AsReadOnly ();
    }
}