using System;
using System.Collections.Generic;
using UnityEngine;

namespace BattleSystem.Configs
{
    [Serializable] public class AbilityInstructionData
    {
        [SerializeField] private string id;
        [SerializeField] private InstructionType instructionType;
        [SerializeField] private List<NameValue> args;
        [SerializeField] private List<NextInstructionData> nextInstructions = new List<NextInstructionData> ();

        public string ID => id;
        public InstructionType InstructionType => instructionType;
        public List<NameValue> Args => args;
        public List<NextInstructionData> NextInstructions => nextInstructions;

        public AbilityInstructionData (string id, string instructionType, List<NameValue> args)
        {
            this.id = id;
            this.instructionType = (InstructionType) Enum.Parse (typeof (InstructionType), instructionType, true);
            this.args = args;
        }
        public AbilityInstructionData (string id, string instructionType, List<NameValue> args, List<NextInstructionData> nextInstructions)
        {
            this.id = id;
            this.instructionType = (InstructionType) Enum.Parse (typeof (InstructionType), instructionType, true);
            this.args = args;
            this.nextInstructions = nextInstructions;
        }
    }

    [Serializable] public class NextInstructionData
    {
        [SerializeField] private string checkResult;

        [SerializeField] private List<AbilityInstructionData> ifInstructions;
        [SerializeField] private List<AbilityInstructionData> elseInstructions;

        public string CheckResult => checkResult;
        public List<AbilityInstructionData> IfInstructions => ifInstructions;
        public List<AbilityInstructionData> ElseInstructions => elseInstructions;

        public NextInstructionData (string checkResult, List<AbilityInstructionData> ifInstructions, List<AbilityInstructionData> elseInstructions)
        {
            this.checkResult = checkResult;
            this.ifInstructions = ifInstructions;
            this.elseInstructions = elseInstructions;
        }
    }

    [Serializable] public class NameValue
    {
        public string name;
        public string value;
        public NameValue (string name, string value)
        {
            this.name = name;
            this.value = value;
        }
    }
}