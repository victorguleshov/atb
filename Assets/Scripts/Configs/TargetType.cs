using System;

namespace BattleSystem.Configs
{
    [Flags] public enum TargetType
    {
        // Everything = -1,
        // Nothing = 0,
        Self = 1 << 0, // только на себя
        Enemy = 1 << 1, // только на врага
        Friend = 1 << 2, // только на союзника
        FriendOrSelf = Friend | Self, // на союзника или на себя
        AOE = 1 << 3, // на группу целей
        AllEnemies = Enemy | AOE, // на всех врагов
        AllFriends = Friend | AOE, // на всех союзников не включая себя
        AllFriendsAndSelf = AllFriends | Self, // на всех союзников включая себя
    }
}