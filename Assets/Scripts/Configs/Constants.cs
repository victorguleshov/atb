using Stats;

namespace BattleSystem.Configs
{
    public static class Constants
    {

        // Stats
        public static readonly IDString MAX_HP = new IDString ("MAX_HP");
        public static readonly IDString STRENGTH = new IDString ("STR");
        public static readonly IDString FORTITUDE = new IDString ("FTD");
        public static readonly IDString SPEED = new IDString ("SPD");
        public static readonly IDString MAX_PREPARATION = new IDString ("MAX_PRP");

        // Attributes
        public static readonly IDString HP = new IDString ("HP");
        public static readonly IDString PREPARATION = new IDString ("PRP");

        // Args
        public static readonly IDString TargetType = new IDString ("t");
    }

    public enum InstructionType
    {
        ChangeAttribute,
        AddModifier,
        RemoveModifier,
        AddStatusEffect,
        RemoveStatusEffect
    }
}