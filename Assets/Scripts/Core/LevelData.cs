using System;
using System.Collections.Generic;
using UnityEngine.Serialization;

namespace BattleSystem.Core
{
    [Serializable] public class LevelData
    {
        public List<Character> characters;

        public LevelData (List<Character> characters)
        {
            this.characters = characters;
        }
        public LevelData () { }
    }

    [Serializable] public class PartyData
    {
        public List<Character> characters;
        public PartyData (List<Character> characters)
        {
            this.characters = characters;
        }
        public PartyData () { }
    }
}