using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Stats;
using Stats.Math;

namespace BattleSystem.Configs
{
    public class AbilityFactory
    {
        private readonly AbilityInstructionsConfig abilityInstructions;

        public AbilityFactory(AbilityInstructionsConfig abilityInstructions)
        {
            this.abilityInstructions = abilityInstructions;
        }
        public ReadOnlyCollection<AbilityInstructionData> Abilities => abilityInstructions.Abilities;

        public bool TryGetAbilityInstruction (string abilityID, out AbilityInstructionData abilityInstruction) =>
            abilityInstructions.Abilities.TryGetFirst (x => x.ID.Equals (abilityID, StringComparison.InvariantCultureIgnoreCase), out abilityInstruction);

        public bool TryGetAbility (string abilityID, out AbilityInstruction ability)
        {
            ability = null;
            return abilityInstructions.Abilities.TryGetFirst (x => x.ID.Equals (abilityID, StringComparison.InvariantCultureIgnoreCase), out var abilityInstruction)
                   && TryGetAbility (abilityInstruction, out ability);
        }
        public bool TryGetAbility (AbilityInstructionData instruction, out AbilityInstruction ability)
        {
            ability = null;
            var nextInstructionsList = new List<AbilityInstruction.Next> ();

            if (instruction.NextInstructions != null)
            {
                foreach (var nextInstructionData in instruction.NextInstructions)
                {
                    nextInstructionsList.Add (new AbilityInstruction.Next (
                        new Comparison (nextInstructionData.CheckResult),
                        GetAbilities (nextInstructionData.IfInstructions),
                        GetAbilities (nextInstructionData.ElseInstructions)
                    ));
                }
            }

            switch (instruction.InstructionType)
            {
                case InstructionType.ChangeAttribute:
                {
                    IDString attributeID = default;
                    ChangeDecimal.Operation change = default;
                    Formula formula = default;
                    var args = new Dictionary<IDString, object> ();

                    foreach (var x in instruction.Args)
                    {
                        if (x.name == "targetType") args.Add (new IDString (x.name), (TargetType) Enum.Parse (typeof (TargetType), x.value, true));
                        else if (x.name == "attributeID") attributeID = new IDString (x.value);
                        else if (x.name == "change") Enum.TryParse (x.value, out change);
                        else if (x.name == "formula") formula = new Formula (x.value);
                        else args.Add (new IDString (x.name), x.value);
                    }

                    ability = new ChangeAttributeInstruction (instruction.ID, nextInstructionsList, args, attributeID, change, formula);
                    return true;
                }
                case InstructionType.AddModifier:
                {
                    IDString statID = default;
                    ModifierType modType = default;
                    Formula formula = default;
                    var args = new Dictionary<IDString, object> ();

                    foreach (var x in instruction.Args)
                    {
                        if (x.name == "targetType") args.Add (new IDString (x.name), (TargetType) Enum.Parse (typeof (TargetType), x.value, true));
                        else if (x.name == "statID") statID = new IDString (x.value);
                        else if (x.name == "modType") Enum.TryParse (x.value, out modType);
                        else if (x.name == "formula") formula = new Formula (x.value);
                        else args.Add (new IDString (x.name), x.value);
                    }

                    ability = new AddModifierInstruction (instruction.ID, nextInstructionsList, args, statID, modType, formula);
                    return true;
                }
                case InstructionType.RemoveModifier:
                {
                    IDString statID = default;
                    ModifierType modType = default;
                    Formula formula = default;
                    var args = new Dictionary<IDString, object> ();

                    foreach (var x in instruction.Args)
                    {
                        if (x.name == "targetType") args.Add (new IDString (x.name), (TargetType) Enum.Parse (typeof (TargetType), x.value, true));
                        else if (x.name == "statID") statID = new IDString (x.value);
                        else if (x.name == "modType") Enum.TryParse (x.value, out modType);
                        else if (x.name == "formula") formula = new Formula (x.value);
                        else args.Add (new IDString (x.name), x.value);
                    }

                    ability = new RemoveModifierInstruction (instruction.ID, nextInstructionsList, args, statID, modType, formula);
                    return true;
                }
                default:
                    return false;
            }
        }

        public List<AbilityInstruction> GetAbilities (IEnumerable<AbilityInstructionData> instructions)
        {
            var result = new List<AbilityInstruction> ();
            foreach (var item in instructions)
            {
                if (TryGetAbility (item, out var ability))
                {
                    result.Add (ability);
                }
            }
            return result;
        }
    }
}