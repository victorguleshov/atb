using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ATB;
using BattleSystem.Configs;
using Stats;
using V.Collections;
using Constants = BattleSystem.Configs.Constants;
using TargetType = BattleSystem.Configs.TargetType;

namespace BattleSystem.Core
{
    public class CastOrdersSystem
    {
        public float waitScaleSize = 0.75f;

        private AbilityFactory abilityFactory;

        public GameState GameState;

        private PriorityQueue<Character> characterSelectCastQueue;
        private List<Character> allCharactersList;

        public ReadOnlyPriorityQueue<Character> CharacterSelectCastQueue { get; private set; }
        public ReadOnlyCollection<Character> AllCharactersList { get; private set; }

        public CastOrdersSystem (PartyData partyData, LevelData levelData, AbilityFactory abilityFactory)
        {
            this.abilityFactory = abilityFactory;

            allCharactersList = new List<Character> ();

            allCharactersList.AddRange (partyData.characters);
            allCharactersList.AddRange (levelData.characters);

            AllCharactersList = allCharactersList.AsReadOnly ();

            characterSelectCastQueue = new PriorityQueue<Character> ();
            CharacterSelectCastQueue = characterSelectCastQueue.AsReadOnly ();
        }

        public event Action UpdateCompletedEvent;

        public void MainLoop (float deltaTime)
        {
            if (GameState != GameState.Cast)
            {
                if (characterSelectCastQueue.Count > 0) // когда игрок закончил выбирать, нужно разрешить оставшиеся в очереди действия
                {
                    ResolveCastsQueue ();
                }
                else
                {
                    foreach (var character in allCharactersList)
                        SyncLoop (character, deltaTime);

                    ResolveCastsQueue ();
                }
            }
            UpdateCompletedEvent?.Invoke ();
        }

        private void ResolveCastsQueue ()
        {
            while (characterSelectCastQueue.Count > 0)
            {
                var character = characterSelectCastQueue.Dequeue (out var deltaTime);

                var preparationAttribute = character.Traits.RuntimeAttributes.Get (Constants.PREPARATION);
                if (preparationAttribute.Value < waitScaleSize) return;
                if (character.SelectedAbility == null)
                    character.SelectAbility ();
                else
                {
                    RunInstruction (character.SelectedAbility);
                    preparationAttribute.Value = 0;
                    character.SelectedAbility = null;
                }

                if (GameState == GameState.Cast)
                    break;

                SyncLoop (character, deltaTime);
            }
        }

        private void SyncLoop (Character character, float deltaTime)
        {
            var usedDelta = 0f;
            var deltaTimeRemainder = 0f;
            var speedStat = character.Traits.RuntimeStats.Get (Constants.SPEED);
            var preparationAttribute = character.Traits.RuntimeAttributes.Get (Constants.PREPARATION);
            var characterSpeed = speedStat.Value;

            var currentPreparationProgress = preparationAttribute.Value;

            var newPreparationProgress = currentPreparationProgress + characterSpeed * deltaTime;

            if (character.SelectedAbility == null && newPreparationProgress - waitScaleSize * preparationAttribute.MaxValue >= 0)
            {
                newPreparationProgress = waitScaleSize * preparationAttribute.MaxValue;
                usedDelta = (float) ((newPreparationProgress - currentPreparationProgress) / (characterSpeed));
                deltaTimeRemainder = deltaTime - usedDelta;
            }
            else if (character.SelectedAbility != null && newPreparationProgress - preparationAttribute.MaxValue >= 0)
            {
                newPreparationProgress = preparationAttribute.MaxValue;
                usedDelta = (float) ((newPreparationProgress - currentPreparationProgress) / (characterSpeed));
                deltaTimeRemainder = deltaTime - usedDelta;
            }

            preparationAttribute.Value = newPreparationProgress;

            character.Traits.RuntimeStatusEffects.Update (usedDelta);

            if (deltaTimeRemainder > 0)
                characterSelectCastQueue.Enqueue (character, deltaTimeRemainder);
        }

        public void RunInstruction (string sourceID, string targetID, string abilityID)
        {
            if (!allCharactersList.TryGetFirst (x => x.ID == sourceID, out var source))
                throw new Exception ($"Source character <{sourceID}> has not found!");
            if (!source.Traits.IsAlive ())
                throw new Exception ($"Source character <{sourceID}> is dead!");

            if (!allCharactersList.TryGetFirst (x => x.ID == targetID, out var target))
                throw new Exception ($"Target character <{targetID}> has not found!");
            if (!target.Traits.IsAlive ())
                throw new Exception ($"Target character <{targetID}> is dead!");

            if (!source.Abilities.Exists (x => x.ID == abilityID))
                throw new Exception ($"Source character <{sourceID}> has not contained ability <{abilityID}>!");
            if (!abilityFactory.TryGetAbilityInstruction (abilityID, out var abilityData))
                throw new Exception ($"Ability data for <{abilityID}> has not found!");
            if (!abilityFactory.TryGetAbility (abilityData, out var ability))
                throw new Exception ($"Ability instructions for <{abilityID}> has not found!");

            ExecuteInstruction (source, new List<Character> { target }, ability);
        }

        private void RunInstruction (PreparationAbilityData selected)
        {
            if (!abilityFactory.TryGetAbilityInstruction (selected.ability.ID, out var abilityData))
                throw new Exception ($"Ability data for <{selected.ability.ID}> has not found!");
            if (!abilityFactory.TryGetAbility (abilityData, out var ability))
                throw new Exception ($"Ability instructions for <{selected.ability.ID}> has not found!");
            ExecuteInstruction (selected.source, selected.targetsList, ability);
        }

        public void ExecuteInstruction (Character source, List<Character> targetsList, AbilityInstruction instruction)
        {
            foreach (var target in targetsList)
            {
                var instructionResult = instruction.Execute (source.Traits, target.Traits);

                foreach (var nextInstruction in instructionResult.nextInstructions)
                {
                    var nextTargets = GetRandomTargetsByType (source, nextInstruction.GetTargetType ());
                    ExecuteInstruction (source, nextTargets, nextInstruction);
                }
            }
        }

        public List<Character> GetRandomTargetsByType (Character source, TargetType targetType)
        {
            var results = new List<Character> ();
            if (targetType.HasFlag (TargetType.Self))
                results.Add (source);

            if (targetType.HasFlag (TargetType.AOE))
            {
                if (targetType.HasFlag (TargetType.Enemy))
                    results.AddRange (allCharactersList
                        .Where (x => x.FriendlyFlag != source.FriendlyFlag));
                if (targetType.HasFlag (TargetType.Friend))
                    results.AddRange (allCharactersList
                        .Where (x => x.FriendlyFlag != source.FriendlyFlag && x.Traits != source.Traits));
            }
            else
            {
                if (targetType.HasFlag (TargetType.Enemy))
                    results.Add (allCharactersList
                        .Where (x => x.FriendlyFlag != source.FriendlyFlag)
                        .ToList ()
                        .Shuffle ()[0]);
                if (targetType.HasFlag (TargetType.Friend))
                    results.Add (allCharactersList
                        .Where (x => x.FriendlyFlag != source.FriendlyFlag && x.Traits != source.Traits)
                        .ToList ()
                        .Shuffle ()[0]);
            }
            return results;
        }
    }

    public static class TraitsExtensions
    {
        public static bool IsAlive (this Traits traits) => traits.RuntimeAttributes.Get (Constants.HP).Value > 0;
        public static TargetType GetTargetType (this AbilityInstruction ability) => ability.Args.TryGetValue (Constants.TargetType, out var targetType) ? (TargetType) targetType : TargetType.Self;
    }
}