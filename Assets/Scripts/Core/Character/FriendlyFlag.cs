using System;

namespace BattleSystem.Core
{
    /// <summary>
    /// этот enum определяет кто кому друг а кто кому враг. Например, если изменить у врага team1 на team0, то враг будет сражаться на твоей стороне aka "Очарование"
    /// </summary>
    [Flags] public enum FriendlyFlag
    {
        Everything = -1, // дружественный всем
        Nothing = 0, // не дружественный никому
        Team0 = 1 << 0,
        Team1 = 1 << 1,
        Team2 = 1 << 2,
        Team3 = 1 << 3,
    }
}