using System;
using BattleSystem.Configs;
using UnityEngine;

namespace BattleSystem.Core
{
    [Serializable] public class AbilityData
    {
        [SerializeField] private string id;
        [SerializeField] private int skillLevel;

        public AbilityData (string id, int skillLevel = 0)
        {
            this.id = id;
            this.skillLevel = skillLevel;
        }

        public string ID => id;
        public int SkillLevel => skillLevel;
        public TargetType TargetType { get; set; }
    }
}