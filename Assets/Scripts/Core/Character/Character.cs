using System;
using System.Collections.Generic;
using Stats;

namespace BattleSystem.Core
{
    [Serializable] public class Character
    {
        private string id;
        private Traits traits;
        private List<AbilityData> abilities;
        private FriendlyFlag friendlyFlag;

        public Character (string id, Traits traits, List<AbilityData> abilities, FriendlyFlag friendlyFlag)
        {
            this.id = id;
            this.traits = traits;
            this.abilities = abilities;
            this.friendlyFlag = friendlyFlag;
        }

        public string ID => id;
        public Traits Traits => traits;
        public List<AbilityData> Abilities => abilities;
        public FriendlyFlag FriendlyFlag => friendlyFlag;

        public PreparationAbilityData SelectedAbility { get; set; }
        public event Action<Character> SelectSpellForCharacter;
        public void SelectAbility ()
        {
            SelectSpellForCharacter?.Invoke (this);
        }
        
        public void PrepareSpell (AbilityData ability, List<Character> targets)
        {
            if (ability == null) return;

            SelectedAbility = new PreparationAbilityData(ability, this, targets);

            // if (!(ability.Preparation?.Count > 0))
            //     ActivateSpell ();
        }
    }

    public class PreparationAbilityData
    {
        public AbilityData ability;
        public Character source;
        public List<Character> targetsList;
        public PreparationAbilityData (AbilityData ability, Character source, List<Character> targetsList)
        {
            this.ability = ability;
            this.source = source;
            this.targetsList = targetsList;
        }
    }
}