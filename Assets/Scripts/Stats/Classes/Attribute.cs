using System.Runtime.Serialization;
using ProtoBuf;

namespace Stats
{
    [DataContract]
    [ProtoContract]
    public class Attribute
    {
        // MEMBERS: -------------------------------------------------------------------------------

        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)]
        private IDString id;

        [ProtoMember (2)] [DataMember (EmitDefaultValue = false)]
        private double minValue;

        [ProtoMember (3)] [DataMember (EmitDefaultValue = false)]
        private Stat maxValueStat;

        [ProtoMember (4)] [DataMember (EmitDefaultValue = false)]
        private float startPercent = 1f;

        [ProtoMember (5)] [DataMember (EmitDefaultValue = false)]
        private bool isHidden = false;

        // CONSTRUCTOR: ---------------------------------------------------------------------------
        public Attribute (IDString id, double minValue, Stat maxValue, float startPercent = 1f, bool isHidden = false)
        {
            this.id = id;
            this.minValue = minValue;
            this.maxValueStat = maxValue;
            this.startPercent = startPercent;
            this.isHidden = isHidden;
        }

        public Attribute () { }

        // PROPERTIES: ----------------------------------------------------------------------------

        public IDString ID => id;
        public double MinValue => minValue;
        public Stat MaxValue => maxValueStat;
        public float StartPercent => startPercent;
        public bool IsHidden => isHidden;
    }
}