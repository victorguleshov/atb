using System.Runtime.Serialization;
using ProtoBuf;
using Stats.Math;

namespace Stats
{
    [DataContract]
    [ProtoContract]
    public class Formula
    {
        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)]
        private string formula;

        private Table table;

        public Formula (string formula, Table table = null)
        {
            this.formula = formula;
            this.table = table;
        }

        public Formula () { }

        // PROPERTIES: ----------------------------------------------------------------------------

        public bool Exists => !string.IsNullOrEmpty (formula);

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public double Calculate (Traits source, Traits target, params KeyValueArgs[] args)
        {
            return Evaluation.Calculate (
                source, target,
                formula, table, args
            );
        }
    }
}