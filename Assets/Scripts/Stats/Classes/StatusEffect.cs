using System;
using System.Runtime.Serialization;
using ProtoBuf;

namespace Stats
{
    [DataContract]
    [ProtoContract]
    public class StatusEffect
    {
        // MEMBERS: -------------------------------------------------------------------------------

        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)]
        private IDString id;

        [ProtoMember (2)] [DataMember (EmitDefaultValue = false)]
        private StatusEffectType effectType = StatusEffectType.Positive;

        [ProtoMember (3)] [DataMember (EmitDefaultValue = false)]
        private int maxStack = 1;

        [ProtoMember (4)] [DataMember (EmitDefaultValue = false)]
        private float duration = 1.0f;

        internal readonly Action onStart;
        internal readonly Action onEnd;
        internal readonly Action<float> whileActive;

        // CONSTRUCTOR: ---------------------------------------------------------------------------

        public StatusEffect (IDString id,
            StatusEffectType effectType = StatusEffectType.Neutral,
            int maxStack = 1,
            float duration = 0,
            Action onStart = default,
            Action onEnd = default,
            Action<float> whileActive = default)
        {
            this.id = id;
            this.effectType = effectType;
            this.maxStack = maxStack;
            this.duration = duration;

            this.onStart = onStart;
            this.onEnd = onEnd;
            this.whileActive = whileActive;
        }

        public StatusEffect () { }

        // PROPERTIES: ----------------------------------------------------------------------------

        public IDString ID => id;
        public StatusEffectType Type => effectType;
        public bool HasDuration => duration > 0;

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public float GetDuration ()
        {
            return HasDuration
                ? duration
                : -1;
        }

        public int GetMaxStack ()
        {
            return MathUtils.FloorToInt (maxStack);
        }
    }

    [Flags]
    public enum StatusEffectType
    {
        Positive = 1, // Mask 0x001
        Negative = 2, // Mask 0x010
        Neutral = 4 // Mask 0x100
    }
}