using System.Collections.Generic;
using System.Runtime.Serialization;
using ProtoBuf;

namespace Stats
{
    [DataContract]
    [ProtoContract]
    public class TraitClass
    {
        // MEMBERS: -------------------------------------------------------------------------------

        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)]
        private string name;

        [ProtoMember (2)] [DataMember (EmitDefaultValue = false)]
        private string description;

        [ProtoMember (3)] [DataMember (EmitDefaultValue = false)]
        private List<Attribute> attributes = new List<Attribute> ();

        [ProtoMember (4)] [DataMember (EmitDefaultValue = false)]
        private List<Stat> stats = new List<Stat> ();

        // PROPERTIES: ----------------------------------------------------------------------------

        public int AttributesLength => attributes.Count;
        public int StatsLength => stats.Count;

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public string GetName () => name;
        public string GetDescription () => description;

        public Attribute GetAttribute (int index) => attributes[index];
        public Stat GetStat (int index) => stats[index];

        public TraitClass (
            string name = "",
            string description = "",
            List<AttributeData> attributes = null,
            List<Stat> stats = null)
        {
            this.name = name;
            this.description = description;
            this.stats = stats ?? new List<Stat> ();

            var attr = new List<Attribute> ();
            if (attributes != null)
                foreach (var item in attributes)
                {
                    if (stats.TryGetFirst (x => x.ID.Equals (item.MaxValue), out var stat))
                    {
                        attr.Add (new Attribute (item.ID, item.MinValue, stat, item.StartPercent, item.IsHidden));
                    }
                }
            this.attributes = attr;
        }

        public TraitClass () { }

        public class AttributeData
        {
            public readonly IDString ID;
            public readonly double MinValue;
            public readonly IDString MaxValue;
            public readonly float StartPercent;
            public readonly bool IsHidden;
            public AttributeData (IDString id, double minValue, IDString maxValue, float startPercent = 1f, bool isHidden = false)
            {
                ID = id;
                MinValue = minValue;
                MaxValue = maxValue;
                StartPercent = startPercent;
                IsHidden = isHidden;
            }
        }
    }
}