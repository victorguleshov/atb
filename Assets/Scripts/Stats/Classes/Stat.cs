using System.Runtime.Serialization;
using ProtoBuf;

namespace Stats
{
    [DataContract]
    [ProtoContract]
    public class Stat
    {
        // MEMBERS: -------------------------------------------------------------------------------

        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)]
        private IDString id;

        [ProtoMember (2)] [DataMember (EmitDefaultValue = false)]
        private double baseValue = 0f;

        [ProtoMember (3)] [DataMember (EmitDefaultValue = false)]
        private Formula formula = null;

        [ProtoMember (4)] [DataMember (EmitDefaultValue = false)]
        private bool isHidden;

        // CONSTRUCTOR: ---------------------------------------------------------------------------

        public Stat (IDString id, double baseValue = 0, Formula formula = null, bool isHidden = false)
        {
            this.id = id;
            this.baseValue = baseValue;
            this.formula = formula;
            this.isHidden = isHidden;
        }

        public Stat () { }

        // PROPERTIES: ----------------------------------------------------------------------------

        public IDString ID => id;
        public double Base => baseValue;
        public Formula Formula => formula;
        public bool IsHidden => isHidden;
    }
}