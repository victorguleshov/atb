using System;
using System.Collections.Generic;

namespace Stats
{
    public class AddStatusEffectInstruction : AbilityInstruction
    {
        protected IDString skillID;
        private StatusEffect statusEffect;

        public AddStatusEffectInstruction (string id, List<Next> nextInstructions, Dictionary<IDString, object> args, IDString skillID, StatusEffect statusEffect)
            : base (id, nextInstructions, args)
        {
            this.skillID = skillID;
            this.statusEffect = statusEffect;
        }

        protected override KeyValueArgs[] ApplyInstruction (Traits source, Traits target)
        {
            target.RuntimeStatusEffects.Add (statusEffect);

            return Array.Empty<KeyValueArgs> ();
        }
    }
}