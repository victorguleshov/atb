using System;
using System.Collections.Generic;

namespace Stats
{
    public class RemoveModifierInstruction : AbilityInstruction
    {
        private IDString statID;
        private ModifierType modType;
        private Formula formula;

        public IDString StatID => statID;
        public ModifierType ModType => modType;
        public Formula Formula => formula;

        public RemoveModifierInstruction (string id, List<Next> nextInstructions, Dictionary<IDString, object> args, IDString statID, ModifierType modType, Formula formula)
            : base (id, nextInstructions, args)
        {
            this.statID = statID;
            this.modType = modType;
            this.formula = formula;
        }

        protected override KeyValueArgs[] ApplyInstruction (Traits source, Traits target)
        {
            var stat = target.RuntimeStats.Get (statID);
            var formulaResult = formula.Calculate (source, target);
            stat.RemoveModifier (modType, formulaResult);

            return Array.Empty<KeyValueArgs> ();
        }
    }
}