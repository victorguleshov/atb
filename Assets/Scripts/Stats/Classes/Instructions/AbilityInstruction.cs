using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Stats
{
    public abstract class AbilityInstruction
    {
        protected readonly string id;
        protected readonly List<Next> nextInstructions;

        protected AbilityInstruction (string id, List<Next> nextInstructions, Dictionary<IDString, object> args)
        {
            this.id = id;
            this.nextInstructions = nextInstructions;

            this.args = args;
            Args = new ReadOnlyDictionary<IDString, object> (args);
        }

        public string ID => id;

        protected Dictionary<IDString, object> args;
        public ReadOnlyDictionary<IDString, object> Args;

        public Result Execute (Traits source, Traits target)
        {
            var resultArgs = ApplyInstruction (source, target);
            var next = new List<AbilityInstruction> ();
            foreach (var x in nextInstructions)
            {
                if (x.comparison.Calculate (source, target, resultArgs))
                {
                    next.AddRange (x.positiveInstructions);
                }
                {
                    next.AddRange (x.negativeInstructions);
                }
            }
            return new Result (resultArgs, next);
        }
        protected abstract KeyValueArgs[] ApplyInstruction (Traits source, Traits target);

        public class Next
        {
            public readonly Comparison comparison;
            public readonly List<AbilityInstruction> positiveInstructions;
            public readonly List<AbilityInstruction> negativeInstructions;
            public Next (Comparison comparison, List<AbilityInstruction> positiveInstructions, List<AbilityInstruction> negativeInstructions)
            {
                this.comparison = comparison;
                this.positiveInstructions = positiveInstructions;
                this.negativeInstructions = negativeInstructions;
            }
        }

        public class Result
        {
            public readonly KeyValueArgs[] args;
            public readonly List<AbilityInstruction> nextInstructions;

            public Result (KeyValueArgs[] args, List<AbilityInstruction> nextInstructions)
            {
                this.args = args;
                this.nextInstructions = nextInstructions;
            }
        }
    }
}