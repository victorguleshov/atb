using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Stats.Math;

namespace Stats
{
    public class ChangeAttributeInstruction : AbilityInstruction
    {
        public IDString attributeID;
        public ChangeDecimal.Operation change;
        public Formula formula;

        public ChangeAttributeInstruction (string id, List<Next> nextInstructions, Dictionary<IDString, object> args, IDString attributeID, ChangeDecimal.Operation change, Formula formula)
            : base (id, nextInstructions, args)
        {
            this.attributeID = attributeID;
            this.change = change;
            this.formula = formula;
        }

        protected override KeyValueArgs[] ApplyInstruction (Traits source, Traits target)
        {
            var args = base.args.Select (x => new KeyValueArgs (x.Key, x.Value.ToString ()));
            var formulaResult = formula.Calculate ( source, target, args.ToArray ());
            var attribute = target.RuntimeAttributes.Get (attributeID);
            var previousValue = attribute.Value;

            var changeDecimal = new ChangeDecimal (change, formulaResult);
            attribute.Value = (float) changeDecimal.Get (attribute.Value);

            return new[] { new KeyValueArgs ("delta", MathUtils.Abs ((float) previousValue - (float) attribute.Value).ToString (CultureInfo.InvariantCulture)) };
        }
    }
}