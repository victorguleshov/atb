using System;
using System.Runtime.Serialization;
using ProtoBuf;
using Stats.Math;

namespace Stats
{
    [ProtoContract]
    [DataContract]
    public class Comparison
    {
        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)]
        private readonly string leftFormula;
        private readonly string rightFormula;
        private readonly string op;

        public Comparison (string expression)
        {
            if (string.IsNullOrWhiteSpace (expression)) return;
            for (var i = 0; i < expression.Length; i++)
            {
                if (expression[i] == '<' || expression[i] == '>' || expression[i] == '=' || expression[i] == '!')
                {
                    if (i < expression.Length - 1 && expression[i + 1] == '=')
                    {
                        op = expression[i] + "=";
                        break;
                    }

                    op = expression[i].ToString ();
                    break;
                }
            }

            if (string.IsNullOrWhiteSpace (op)) return;

            var splitted = expression.Split (new[] { op }, StringSplitOptions.RemoveEmptyEntries);
            if (splitted.Length != 2)
                throw new ArgumentException ($"Invalid expression {expression}");

            leftFormula = splitted[0];
            rightFormula = splitted[1];
        }

        public Comparison () { }

        public bool Calculate (Traits source, Traits target, params KeyValueArgs[] args)
        {
            if (string.IsNullOrWhiteSpace (op)) return true;

            var left = Evaluation.Calculate (source, target, leftFormula, null, args);
            var right = Evaluation.Calculate (source, target, rightFormula, null, args);
            return op switch
            {
                "<" => left < right,
                "<=" => left <= right,
                ">" => left > right,
                ">=" => left >= right,
                "!=" => System.Math.Abs (left - right) > 0.001f,
                _ => System.Math.Abs (left - right) < 0.001f
            };
        }
    }
}