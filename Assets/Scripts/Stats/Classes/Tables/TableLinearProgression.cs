using System;
using Stats.Tables.Shared;

namespace Stats.Tables
{
    [Serializable]
    public class TableLinearProgression : TTable
    {
        // +--------------------------------------------------------------------------------------+
        // | EXP_Level(n + 1) = EXP_Level(n) + (n * increment)                                    |
        // |                                                                                      |
        // | n: is the current level.                                                             |
        // | increment: is the amount of experience added per level.                              |
        // +--------------------------------------------------------------------------------------+

        private int m_MaxLevel = 99;
        private int m_IncrementPerLevel = 50;

        // PROPERTIES: ----------------------------------------------------------------------------

        public override int MinLevel => 1;
        public override int MaxLevel => m_MaxLevel;

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public TableLinearProgression () : base () { }

        public TableLinearProgression (int maxLevel, int incrementPerLevel) : this ()
        {
            m_MaxLevel = maxLevel;
            m_IncrementPerLevel = incrementPerLevel;
        }

        // IMPLEMENT METHODS: ---------------------------------------------------------------------

        protected override int LevelFromCumulative (int cumulative)
        {
            var squareRoot = MathUtils.Sqrt (1f + 8f * cumulative / m_IncrementPerLevel);
            var level = (1 + squareRoot) / 2.0f;

            return MathUtils.Clamp (MathUtils.FloorToInt (level), MinLevel, MaxLevel + 1);
        }

        protected override int CumulativeFromLevel (int level)
        {
            var power = MathUtils.Pow (level, 2.0f);
            var result = (power - level) * m_IncrementPerLevel / 2.0f;

            return MathUtils.FloorToInt (result);
        }
    }
}