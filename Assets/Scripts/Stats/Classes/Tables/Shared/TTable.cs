using System;

namespace Stats.Tables.Shared
{
    [Serializable]
    public abstract class TTable : ITable
    {
        // PROPERTIES: ----------------------------------------------------------------------------

        public virtual int MinLevel => 1;
        public virtual int MaxLevel => 99;

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public int GetLevelForCumulativeExperience (int cumulative)
        {
            return LevelFromCumulative (cumulative);
        }

        public int GetCumulativeExperienceForLevel (int level)
        {
            level = MathUtils.Clamp (level, MinLevel, MaxLevel + 1);
            return CumulativeFromLevel (level);
        }

        public int GetLevelExperienceForLevel (int level)
        {
            var currLevel = MathUtils.Clamp (level + 0, MinLevel, MaxLevel + 1);
            var nextLevel = MathUtils.Clamp (level + 1, MinLevel, MaxLevel + 1);

            var cumulativeCurrent = CumulativeFromLevel (currLevel);
            var cumulativeNext = CumulativeFromLevel (nextLevel);

            return cumulativeNext - cumulativeCurrent;
        }

        public int GetLevelExperienceAtCurrentLevel (int cumulative)
        {
            var currentLevel = GetLevelForCumulativeExperience (cumulative);
            return cumulative - CumulativeFromLevel (currentLevel);
        }

        public int GetLevelExperienceToNextLevel (int cumulative)
        {
            var nextLevel = GetNextLevel (cumulative);
            return CumulativeFromLevel (nextLevel) - cumulative;
        }

        public float GetRatioAtCurrentLevel (int cumulative)
        {
            var experienceFrom = GetLevelExperienceAtCurrentLevel (cumulative);
            var experienceTo = GetLevelExperienceToNextLevel (cumulative);
            return (float) experienceFrom / (experienceFrom + experienceTo);
        }

        public float GetRatioForNextLevel (int cumulative)
        {
            return 1f - GetRatioAtCurrentLevel (cumulative);
        }

        // PROTECTED METHODS: ---------------------------------------------------------------------

        protected int GetPreviousLevel (int cumulative)
        {
            var currentLevel = LevelFromCumulative (cumulative);
            return System.Math.Max (MinLevel, currentLevel - 1);
        }

        protected int GetNextLevel (int cumulative)
        {
            var currentLevel = LevelFromCumulative (cumulative);
            return System.Math.Min (MaxLevel, currentLevel + 1);
        }

        // ABSTRACT METHODS: ----------------------------------------------------------------------

        protected abstract int LevelFromCumulative (int cumulative);
        protected abstract int CumulativeFromLevel (int level);
    }
}