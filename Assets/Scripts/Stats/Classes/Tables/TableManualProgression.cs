using System;
using Stats.Tables.Shared;

namespace Stats.Tables
{
    [Serializable]
    public class TableManualProgression : TTable
    {
        // +--------------------------------------------------------------------------------------+
        // | EXP_Level(n) = X(n)                                                                  |
        // |                                                                                      |
        // | n: is the current level.                                                             |
        // | X(n): is the n-th variable from the experience table                                 |
        // +--------------------------------------------------------------------------------------+

        private int[] m_Increments = new int[99];

        // PROPERTIES: ----------------------------------------------------------------------------

        public override int MinLevel => 1;
        public override int MaxLevel => m_Increments.Length - 1;

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public TableManualProgression () : base ()
        {
            for (var i = 0; i < m_Increments.Length; ++i)
            {
                m_Increments[i] = 10 + 5 * i;
            }
        }

        public TableManualProgression (int[] increments) : this ()
        {
            m_Increments = increments;
        }

        // IMPLEMENT METHODS: ---------------------------------------------------------------------

        protected override int LevelFromCumulative (int cumulative)
        {
            var sum = 0;

            for (var i = 0; i < m_Increments.Length; ++i)
            {
                if (cumulative < sum) return i + 1;
                sum += m_Increments[i];
            }

            return MaxLevel;
        }

        protected override int CumulativeFromLevel (int level)
        {
            var sum = 0;
            for (var i = 0; i < level - 1; ++i)
            {
                sum += m_Increments[i];
            }

            return sum;
        }
    }
}