using System;
using Stats.Tables.Shared;

namespace Stats.Tables
{
    [Serializable]
    public class TableConstantProgression : TTable
    {
        // +--------------------------------------------------------------------------------------+
        // | EXP_Level(n + 1) = experience                                                        |
        // |                                                                                      |
        // | n: is the current level.                                                             |
        // | experience: is the amount of experience required per level.                          |
        // +--------------------------------------------------------------------------------------+

        private int m_MaxLevel = 99;
        private int m_Increment = 50;

        // PROPERTIES: ----------------------------------------------------------------------------

        public override int MinLevel => 1;
        public override int MaxLevel => m_MaxLevel;

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public TableConstantProgression () : base () { }

        public TableConstantProgression (int maxLevel, int increment) : this ()
        {
            m_MaxLevel = maxLevel;
            m_Increment = increment;
        }

        // IMPLEMENT METHODS: ---------------------------------------------------------------------

        protected override int LevelFromCumulative (int cumulative)
        {
            var level = (float) cumulative / m_Increment;
            return MathUtils.Clamp (MathUtils.FloorToInt (level), MinLevel, MaxLevel + 1);
        }

        protected override int CumulativeFromLevel (int level)
        {
            return level * m_Increment;
        }
    }
}