using System;
using Stats.Tables.Shared;

namespace Stats.Tables
{
    [Serializable]
    public class TableGeometricProgression : TTable
    {
        // +--------------------------------------------------------------------------------------+
        // | EXP_Level(n + 1) = EXP_Level(n) * rate                                               |
        // |                                                                                      |
        // | n: is the current level.                                                             |
        // | rate: the incremental ratio of experience from the previous level                    |
        // +--------------------------------------------------------------------------------------+

        private int m_MaxLevel = 99;
        private int m_Increment = 50;
        private float m_Rate = 1.05f;

        // PROPERTIES: ----------------------------------------------------------------------------

        public override int MinLevel => 1;
        public override int MaxLevel => m_MaxLevel;

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public TableGeometricProgression () : base () { }

        public TableGeometricProgression (int maxLevel, int increment, float rate) : this ()
        {
            m_MaxLevel = maxLevel;
            m_Increment = increment;
            m_Rate = rate;
        }

        // IMPLEMENT METHODS: ---------------------------------------------------------------------

        protected override int LevelFromCumulative (int cumulative)
        {
            var logValue = cumulative * (m_Rate - 1f) / m_Increment;
            return MathUtils.FloorToInt (MathUtils.Log (logValue + 1f, m_Rate)) + 1;
        }

        protected override int CumulativeFromLevel (int level)
        {
            var value = (1f - MathUtils.Pow (m_Rate, level - 1)) / (1f - m_Rate);
            return MathUtils.FloorToInt (m_Increment * value);
        }
    }
}