using System;
using System.Runtime.Serialization;
using ProtoBuf;
#if UNITY
using UnityEngine;
#endif

namespace Stats
{
    [DataContract]
    [ProtoContract]
#if UNITY
    [Serializable]
#endif
    public struct IDString : IEquatable<IDString>
    {
        public static readonly IDString EMPTY = new IDString (string.Empty);

        // EXPOSED MEMBERS: -----------------------------------------------------------------------

        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)]
#if UNITY
        [SerializeField]
#endif
        private string str;

        // MEMBERS: -------------------------------------------------------------------------------

        private int hash;

        // PROPERTIES: ----------------------------------------------------------------------------

        public string String => str ?? string.Empty;

        public int Hash
        {
            get
            {
                if (hash == 0) hash = String.GetHashCode ();
                return hash;
            }
        }

        // CONSTRUCTOR: ---------------------------------------------------------------------------

        public IDString (string value)
        {
            str = value;
            hash = 0;
        }

        // OVERRIDES: -----------------------------------------------------------------------------

        public override int GetHashCode () => Hash;

        public bool Equals (IDString other)
        {
            return Hash == other.Hash;
        }

        public override bool Equals (object other)
        {
            return other is IDString otherIdString && Equals (otherIdString);
        }

        public static implicit operator string (IDString @this) => @this.String;
        public static implicit operator int (IDString @this) => @this.Hash;
        public static explicit operator IDString (string @this) => new IDString (@this);

        public override string ToString ()
        {
            return str;
        }
    }
}