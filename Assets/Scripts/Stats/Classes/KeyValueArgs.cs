namespace Stats
{
    public class KeyValueArgs
    {
        public string name;
        public string value;

        public KeyValueArgs (string name, string value)
        {
            this.name = name;
            this.value = value;
        }
    }
}