using System.Runtime.Serialization;
using ProtoBuf;

namespace Stats
{
    [DataContract] [ProtoContract]
    public class OverrideStatData
    {
        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)] private bool changeBase = false;
        [ProtoMember (2)] [DataMember (EmitDefaultValue = false)] private double baseValue = 1;

        // PROPERTIES: ----------------------------------------------------------------------------

        public OverrideStatData (double baseValue)
        {
            changeBase = true;
            this.baseValue = baseValue;
        }

        public OverrideStatData ()
        {
            changeBase = false;
        }

        public bool ChangeBase => changeBase;
        public double BaseValue => baseValue;
    }
}