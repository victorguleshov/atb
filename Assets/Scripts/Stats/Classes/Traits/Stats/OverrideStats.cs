using System.Collections.Generic;

namespace Stats
{
    public static class OverrideStatsExtensions
    {
        public static Dictionary<IDString, OverrideStatData> SyncWithClass (
            this Dictionary<IDString, OverrideStatData> overrideStats, TraitClass traitTraitClass)
        {
            Dictionary<IDString, OverrideStatData> data =
                new Dictionary<IDString, OverrideStatData> ();

            for (int i = 0; i < traitTraitClass.StatsLength; ++i)
            {
                Stat stat = traitTraitClass.GetStat (i);
                if (stat == null || stat.IsHidden) continue;

                overrideStats.TryGetValue (stat.ID, out OverrideStatData entry);
                data[stat.ID] = entry ?? new OverrideStatData ();
            }

            return data;
        }
    }
}