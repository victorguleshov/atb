using System;
using System.Collections.Generic;

namespace Stats
{
    [Serializable]
    public class RuntimeStats
    {
        // MEMBERS: -------------------------------------------------------------------------------

        private Traits m_Traits;
        private Dictionary<int, RuntimeStatData> m_Stats;

        // PROPERTIES: ----------------------------------------------------------------------------

        public int Count => m_Stats.Count;

        /// <summary>
        /// Returns the difference between the the previous and new value from the last
        /// modified Stat.
        /// </summary>
        public double LastChange { get; private set; }

        public List<int> StatsKeys => new List<int> (m_Stats.Keys);

        // EVENTS: --------------------------------------------------------------------------------

        public event Action<IDString> EventChange;

        // CONSTRUCTOR: ---------------------------------------------------------------------------

        internal RuntimeStats (Traits traits)
        {
            m_Traits = traits;
            m_Stats = new Dictionary<int, RuntimeStatData> ();
        }

        internal RuntimeStats (Traits traits, Dictionary<IDString, OverrideStatData> overrideStats) : this (traits)
        {
            for (int i = 0; i < m_Traits.TraitClass.StatsLength; ++i)
            {
                Stat stat = m_Traits.TraitClass.GetStat (i);
                if (stat == null || stat == null)
                {
                    const string error = "No Stat reference found";
                    throw new NullReferenceException (error);
                }

                IDString statID = stat.ID;
                if (m_Stats.ContainsKey (statID.Hash))
                {
                    string error = $"Duplicate Stat '{statID.String}' has already been defined";
                    throw new Exception (error);
                }

                RuntimeStatData data = new RuntimeStatData (m_Traits, stat);
                if (!stat.IsHidden &&
                    overrideStats.TryGetValue (statID, out OverrideStatData overrideData))
                {
                    if (overrideData.ChangeBase) data.Base = overrideData.BaseValue;
                }

                data.EventChange += ExecuteEventChange;
                m_Stats[statID.Hash] = data;
            }
        }

        // PRIVATE METHODS: -----------------------------------------------------------------------

        private void ExecuteEventChange (IDString statID, double change)
        {
            LastChange = change;
            EventChange?.Invoke (statID);
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        /// <summary>
        /// Returns the RuntimeStatData instance of a requested stat. Throws an exception if the
        /// requested stat cannot be found.
        /// </summary>
        /// <param name="statID"></param>
        /// <returns></returns>
        public RuntimeStatData Get (IDString statID)
        {
            if (m_Stats == null) return null;
            if (m_Stats.TryGetValue (statID.Hash, out RuntimeStatData stat))
            {
                return stat;
            }

            // string objectName = this.m_Traits.gameObject.name;
            throw new Exception ($"Cannot find Stat '{statID.String}'"); // in {objectName}");
        }

        /// <summary>
        /// Returns the RuntimeStatData instance of a requested stat. Throws an exception if the
        /// requested stat cannot be found.
        /// </summary>
        /// <param name="statID"></param>
        /// <returns></returns>
        public RuntimeStatData Get (string statID)
        {
            return Get (new IDString (statID));
        }

        // INTERNAL METHODS: ----------------------------------------------------------------------

        /// <summary>
        /// Returns the RuntimeStatData instance of a requested stat.
        /// </summary>
        /// <param name="statHash"></param>
        /// <returns></returns>
        public RuntimeStatData Get (int statHash)
        {
            return m_Stats.TryGetValue (statHash, out RuntimeStatData stat) ? stat : null;
        }
    }
}