using System;

namespace Stats
{
    public class RuntimeStatData
    {
        private readonly Traits m_Traits;
        private readonly Stat m_Stat;
        private readonly Modifiers m_Modifiers;

        private double m_Base;
        private readonly Formula m_Formula;

        // PROPERTIES: ----------------------------------------------------------------------------

        /// <summary>
        /// The unprocessed value of the stat. No formula nor modifiers are applied.
        /// </summary>
        public double Base
        {
            get => m_Base;
            set
            {
                if (System.Math.Abs (m_Base - value) < float.Epsilon) return;

                double prevValue = Value;
                m_Base = value;

                EventChange?.Invoke (m_Stat.ID, Value - prevValue);
            }
        }

        /// <summary>
        /// The stat value with the formula and stat modifiers applied.
        /// </summary>
        public double Value
        {
            get
            {
                double value = m_Formula != null && m_Formula.Exists
                    ? m_Formula.Calculate (m_Traits, m_Traits)
                    : m_Base;

                return m_Modifiers.Calculate (value);
            }
        }

        /// <summary>
        /// The amount modifiers contribute to the resulting stat value. 
        /// </summary>
        public double ModifiersValue
        {
            get
            {
                double value = m_Formula != null && m_Formula.Exists
                    ? m_Formula.Calculate (m_Traits, m_Traits)
                    : m_Base;

                return m_Modifiers.Calculate (value) - value;
            }
        }
        
        public IDString ID => m_Stat.ID;

        // EVENTS: --------------------------------------------------------------------------------

        public event Action<IDString, double> EventChange;

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public RuntimeStatData (Traits traits, Stat stat)
        {
            m_Traits = traits;
            m_Stat = stat;
            m_Modifiers = new Modifiers (stat.ID.Hash);

            m_Base = stat.Base;
            m_Formula = stat.Formula;
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        /// <summary>
        /// Adds a constant stat modifier that increments or decrements the resulting stat value
        /// </summary>
        /// <example>Constant stat modifiers simply add up to the stat value</example>
        /// <example>Percentage stat modifiers increase the value once all other constant stat
        /// modifiers have been added. A value of 0.1 increases the stat by 10%</example>
        /// <param name="type"></param>
        /// <param name="value"></param>
        public void AddModifier (ModifierType type, double value)
        {
            switch (type)
            {
                case ModifierType.Constant:
                    m_Modifiers.AddConstant (value);
                    break;
                case ModifierType.Percent:
                    m_Modifiers.AddPercentage (value);
                    break;
                default: throw new ArgumentOutOfRangeException (nameof (type), type, null);
            }

            EventChange?.Invoke (m_Stat.ID, 0f);
        }

        /// <summary>
        /// Removes a stat modifier (or equivalent) from the current list of modifiers. Returns
        /// true if a similar stat modifier is found and successfully removed. False otherwise.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool RemoveModifier (ModifierType type, double value)
        {
            bool success = type switch
            {
                ModifierType.Constant => m_Modifiers.RemoveConstant (value),
                ModifierType.Percent => m_Modifiers.RemovePercentage (value),
                _ => throw new ArgumentOutOfRangeException (nameof (type), type, null)
            };

            if (success) EventChange?.Invoke (m_Stat.ID, 0f);
            return success;
        }
    }
}