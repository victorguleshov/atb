using System;
using System.Collections.Generic;

namespace Stats
{
    public class RuntimeStatusEffects
    {
        // MEMBERS: -------------------------------------------------------------------------------

        private Traits m_Traits;
        private Dictionary<int, RuntimeStatusEffectList> m_Active;

        // PROPERTIES: ----------------------------------------------------------------------------

        public int Count => m_Active.Count;

        // EVENTS: --------------------------------------------------------------------------------

        public event Action<IDString> EventChange;

        // CONSTRUCTORS: --------------------------------------------------------------------------

        internal RuntimeStatusEffects (Traits traits)
        {
            m_Traits = traits;
            m_Active = new Dictionary<int, RuntimeStatusEffectList> ();
        }

        // INTERNAL METHODS: ----------------------------------------------------------------------

        public void Update (float deltaTime)
        {
            if (m_Active == null) return;
            foreach (var entry in m_Active)
            {
                entry.Value?.Update (deltaTime);
            }
        }

        // PRIVATE METHODS: -----------------------------------------------------------------------

        private void ExecuteEventChange (IDString statusEffectID)
        {
            EventChange?.Invoke (statusEffectID);
        }

        // SETTERS: -------------------------------------------------------------------------------

        /// <summary>
        /// Adds a new Status Effect onto the Traits target. If the max stack is full, it removes
        /// the oldest Status Effect and creates a new one.
        /// </summary>
        /// <param name="statusEffect"></param>
        /// <param name="timeElapsed"></param>
        public void Add (StatusEffect statusEffect, float timeElapsed = 0f)
        {
            if (statusEffect == null) return;

            if (!m_Active.TryGetValue (statusEffect.ID.Hash, out var list))
            {
                list = new RuntimeStatusEffectList (m_Traits, statusEffect);
                list.EventChange += ExecuteEventChange;

                m_Active.Add (statusEffect.ID.Hash, list);
            }

            list.Add (timeElapsed);
        }

        /// <summary>
        /// Removes the oldest active Status Effect of the specified type.
        /// </summary>
        /// <param name="statusEffect"></param>
        /// <param name="amount"></param>
        public void Remove (StatusEffect statusEffect, int amount = 1)
        {
            if (m_Active.TryGetValue (statusEffect.ID.Hash, out var list))
            {
                list.Remove (amount);
            }
        }

        /// <summary>
        /// Removes all active Status Effects that match the type mask.
        /// </summary>
        /// <param name="maskType"></param>
        public void ClearByType (StatusEffectType maskType)
        {
            foreach (var entry in m_Active)
            {
                entry.Value?.RemoveByType ((int) maskType);
            }
        }

        // GETTERS: -------------------------------------------------------------------------------

        /// <summary>
        /// Returns a list of all active status effects.
        /// </summary>
        /// <returns></returns>
        public List<IDString> GetActiveList ()
        {
            var active = new List<IDString> ();

            if (m_Active == null) return active;
            foreach (var entry in m_Active)
            {
                if (entry.Value.Count == 0) continue;
                active.Add (entry.Value.ID);
            }

            return active;
        }

        /// <summary>
        /// Returns the Status Effect asset of an active status effects given a particular ID
        /// </summary>
        /// <param name="statusEffectID"></param>
        /// <returns></returns>
        public StatusEffect GetActiveStatusEffect (IDString statusEffectID)
        {
            return m_Active.TryGetValue (statusEffectID.Hash, out var list)
                ? list.StatusEffect
                : null;
        }

        /// <summary>
        /// Returns the amount of active stacked status effects given a particular ID
        /// </summary>
        /// <param name="statusEffectID"></param>
        /// <returns></returns>
        public int GetActiveStackCount (IDString statusEffectID)
        {
            return m_Active.TryGetValue (statusEffectID.Hash, out var list)
                ? list.Count
                : 0;
        }

        public StatusEffect GetActiveStackStatusEffect (IDString statusEffectID)
        {
            return m_Active.TryGetValue (statusEffectID.Hash, out var list)
                ? list.StatusEffect
                : null;
        }

        /// <summary>
        /// Returns the active stacked status effect at the specified index. 
        /// </summary>
        /// <param name="statusEffectID"></param>
        /// <param name="stackIndex"></param>
        /// <returns></returns>
        public RuntimeStatusEffectValue GetActiveAt (IDString statusEffectID, int stackIndex)
        {
            return m_Active.TryGetValue (statusEffectID.Hash, out var list)
                ? list.GetValueAt (stackIndex)
                : default;
        }
    }
}