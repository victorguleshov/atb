using System.Runtime.Serialization;
using ProtoBuf;

namespace Stats
{
    [DataContract] [ProtoContract]
    public struct RuntimeStatusEffectValue
    {
        // MEMBERS: -------------------------------------------------------------------------------

        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)] private IDString m_ID;

        [ProtoMember (2)] [DataMember (EmitDefaultValue = false)] private double m_TimeElapsed;
        [ProtoMember (3)] [DataMember (EmitDefaultValue = false)] private double m_TimeRemaining;

        // CONSTRUCTOR: ---------------------------------------------------------------------------

        public RuntimeStatusEffectValue (string id, double timeElapsed, double timeRemaining)
        {
            m_ID = new IDString (id);

            m_TimeElapsed = timeElapsed;
            m_TimeRemaining = timeRemaining;
        }

        // PROPERTIES: ----------------------------------------------------------------------------

        public double TimeElapsed => m_TimeElapsed;
        public double TimeRemaining => m_TimeRemaining;

        public bool HasDuration => TimeRemaining >= 0f;
        public double GetDuration => TimeElapsed + TimeRemaining;

        public double Progress => TimeElapsed / (TimeElapsed + TimeRemaining);
    }
}