using System;
using System.Collections.Generic;

namespace Stats
{
    internal class RuntimeStatusEffectList
    {
        // MEMBERS: -------------------------------------------------------------------------------

        private readonly List<RuntimeStatusEffectData> m_List = new List<RuntimeStatusEffectData> ();

        private readonly Traits m_Traits;
        private readonly StatusEffect m_StatusEffect;

        // PROPERTIES: ----------------------------------------------------------------------------

        public int Count => m_List.Count;

        public IDString ID => m_StatusEffect.ID;
        public StatusEffect StatusEffect => m_StatusEffect;

        // EVENTS: --------------------------------------------------------------------------------

        public event Action<IDString> EventChange;

        public event Action<IDString> EventAdd;
        public event Action<IDString> EventRemove;

        // CONSTRUCTOR: ---------------------------------------------------------------------------

        public RuntimeStatusEffectList (Traits traits, StatusEffect statusEffect)
        {
            m_Traits = traits;
            m_StatusEffect = statusEffect;
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public void Update (float deltaTime)
        {
            var removeAny = false;

            for (var i = m_List.Count - 1; i >= 0; --i)
            {
                var data = m_List[i];
                if (data.Update (deltaTime))
                {
                    data.Stop ();
                    m_List.RemoveAt (i);
                    removeAny = true;
                }
            }

            if (removeAny)
            {
                EventRemove?.Invoke (m_StatusEffect.ID);
                EventChange?.Invoke (m_StatusEffect.ID);
            }
        }

        public void Add (float timeElapsed)
        {
            var maxStack = m_StatusEffect.GetMaxStack ();
            if (maxStack <= 0) return;

            if (Count >= maxStack)
            {
                m_List[0].Stop ();
                m_List.RemoveAt (0);
                EventRemove?.Invoke (m_StatusEffect.ID);
            }

            m_List.Add (new RuntimeStatusEffectData (
                m_Traits,
                m_StatusEffect,
                timeElapsed
            ));

            EventAdd?.Invoke (m_StatusEffect.ID);
            EventChange?.Invoke (m_StatusEffect.ID);
        }

        public void Remove (int amount = 1)
        {
            if (Count <= 0) return;
            while (Count > 0 && amount > 0)
            {
                m_List[0].Stop ();
                m_List.RemoveAt (0);

                amount -= 1;
            }

            EventRemove?.Invoke (m_StatusEffect.ID);
            EventChange?.Invoke (m_StatusEffect.ID);
        }

        public void RemoveByType (int maskType)
        {
            if (((int) m_StatusEffect.Type & maskType) == 0) return;
            for (var i = Count - 1; i >= 0; --i)
            {
                m_List[i].Stop ();
                m_List.RemoveAt (i);
            }

            EventRemove?.Invoke (m_StatusEffect.ID);
            EventChange?.Invoke (m_StatusEffect.ID);
        }

        public RuntimeStatusEffectValue GetValueAt (int index)
        {
            return index < m_List.Count
                ? m_List[index].GetValue ()
                : default;
        }
    }
}