namespace Stats
{
    internal class RuntimeStatusEffectData
    {
        // MEMBERS: -------------------------------------------------------------------------------

        private readonly Traits traits;
        private readonly StatusEffect statusEffect;

        private readonly float duration;
        private float elapsedTime;

        // CONSTRUCTOR: ---------------------------------------------------------------------------

        public RuntimeStatusEffectData (Traits traits, StatusEffect statusEffect, float elapsedTime = 0f)
        {
            this.elapsedTime = elapsedTime;
            duration = statusEffect.GetDuration ();

            this.traits = traits;
            this.statusEffect = statusEffect;

            RunOnStart ();
            RunWhileActive ();
        }

        // INTERNAL METHODS: ----------------------------------------------------------------------

        public bool Update (float deltaTime)
        {
            if (!statusEffect.HasDuration) return false;
            elapsedTime += deltaTime;

            var result = elapsedTime > duration;

            if (!result)
            {
                RunWhileActive ();
            }

            return result;
        }

        // PRIVATE METHODS: -----------------------------------------------------------------------

        private void RunOnStart ()
        {
            statusEffect.onStart?.Invoke ();
        }

        private void RunOnEnd ()
        {
            statusEffect.onEnd?.Invoke ();
        }

        private void RunWhileActive ()
        {
            statusEffect.whileActive?.Invoke (elapsedTime);
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public void Stop ()
        {
            RunOnEnd ();
        }

        public RuntimeStatusEffectValue GetValue ()
        {
            if (statusEffect.HasDuration)
            {
                return new RuntimeStatusEffectValue (
                    statusEffect.ID.String,
                    elapsedTime, statusEffect.GetDuration () - elapsedTime
                );
            }

            return new RuntimeStatusEffectValue (
                statusEffect.ID.String,
                elapsedTime, -1f
            );
        }
    }
}