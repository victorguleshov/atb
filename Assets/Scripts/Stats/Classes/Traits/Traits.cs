using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using ProtoBuf;

namespace Stats
{
    [DataContract]
    [ProtoContract]
    public class Traits
    {
        private const string ERR_NO_CLASS = "Traits component has no Class reference";

        // EXPOSED MEMBERS: -----------------------------------------------------------------------

        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)]
        private TraitClass traitClass;

        [ProtoMember (2)] [DataMember (EmitDefaultValue = false)]
        private Dictionary<IDString, OverrideAttributeData> overrideAttributes =
            new Dictionary<IDString, OverrideAttributeData> ();

        [ProtoMember (3)] [DataMember (EmitDefaultValue = false)]
        private Dictionary<IDString, OverrideStatData> overrideStats = new Dictionary<IDString, OverrideStatData> ();

        // MEMBERS: -------------------------------------------------------------------------------

        private RuntimeStats runtimeStats;

        private RuntimeAttributes runtimeAttributes;
        private RuntimeStatusEffects runtimeStatusEffects;

        // PROPERTIES: ----------------------------------------------------------------------------

        public Traits (
            TraitClass traitClass = null,
            Dictionary<IDString, OverrideStatData> overrideStats = null,
            Dictionary<IDString, OverrideAttributeData> overrideAttributes = null)
        {
            this.traitClass = traitClass;
            this.overrideStats = overrideStats ?? new Dictionary<IDString, OverrideStatData> ();
            this.overrideAttributes = overrideAttributes ?? new Dictionary<IDString, OverrideAttributeData> ();

            RuntimeInitStats ();
            RuntimeInitAttributes ();
        }

        private Traits () { }

        public TraitClass TraitClass => traitClass;

        public RuntimeStats RuntimeStats
        {
            get
            {
                RuntimeInitStats ();
                return runtimeStats;
            }
        }

        public RuntimeAttributes RuntimeAttributes
        {
            get
            {
                RuntimeInitAttributes ();
                return runtimeAttributes;
            }
        }

        public RuntimeStatusEffects RuntimeStatusEffects
        {
            get
            {
                RuntimeInitStatusEffects ();
                return runtimeStatusEffects;
            }
        }

        // EVENTS: --------------------------------------------------------------------------------

        public event Action EventChange;

        // UPDATE METHODS: ------------------------------------------------------------------------

        // private void Update()
        // {
        //     this.RuntimeStatusEffects.Update();
        // }

        // INITIALIZERS: --------------------------------------------------------------------------

        private void Awake ()
        {
            RuntimeInitStats ();
            RuntimeInitAttributes ();
            RuntimeInitStatusEffects ();
        }

        private void RuntimeInitStats ()
        {
            if (runtimeStats != null) return;

            if (traitClass == null)
            {
                runtimeStats = new RuntimeStats (this);

                // Debug.LogError(ERR_NO_CLASS, this);
                return;
            }

            runtimeStats = new RuntimeStats (this, overrideStats);
            runtimeStats.EventChange += _ => EventChange?.Invoke ();
        }

        private void RuntimeInitAttributes ()
        {
            if (runtimeAttributes != null) return;

            if (traitClass == null)
            {
                runtimeAttributes = new RuntimeAttributes (this);

                // Debug.LogError(ERR_NO_CLASS, this);
                return;
            }

            runtimeAttributes = new RuntimeAttributes (this, overrideAttributes);
            runtimeAttributes.EventChange += _ => EventChange?.Invoke ();
        }

        private void RuntimeInitStatusEffects ()
        {
            if (runtimeStatusEffects != null) return;

            runtimeStatusEffects = new RuntimeStatusEffects (this);
            runtimeStatusEffects.EventChange += _ => EventChange?.Invoke ();
        }

        // STATIC METHODS: ------------------------------------------------------------------------

        // public static Traits SetTraitsWithClass(GameObject target, Class assetClass)
        // {
        //     Traits traits = target.Add<Traits>();
        //     traits.m_Class = assetClass;
        //
        //     traits.m_RuntimeStats = null;
        //     // traits.m_RuntimeAttributes = null;
        //     
        //     traits.RuntimeInitStats();
        //     // traits.RuntimeInitAttributes();
        //
        //     return traits;
        // }

        // SERIALIZATION CALLBACKS: ---------------------------------------------------------------

        // void ISerializationCallbackReceiver.OnAfterDeserialize()
        // { }
        //
        // void ISerializationCallbackReceiver.OnBeforeSerialize()
        // {
        //     if (this.m_Class == null) return;
        //
        //     m_OverrideAttributes = this.m_OverrideAttributes.SyncWithClass(this.m_Class);
        //     m_OverrideStats = this.m_OverrideStats.SyncWithClass(this.m_Class);
        // }
    }
}