using System;
using System.Collections.Generic;

namespace Stats
{
    [Serializable]
    public class RuntimeAttributes
    {
        // MEMBERS: -------------------------------------------------------------------------------

        private Traits m_Traits;
        private Dictionary<int, RuntimeAttributeData> m_Attributes;

        // PROPERTIES: ----------------------------------------------------------------------------

        public int Count => m_Attributes.Count;

        /// <summary>
        /// Returns the difference between the the previous and new value from the last
        /// modified Attribute.
        /// </summary>
        public double LastChange { get; private set; }

        public List<int> AttributesKeys => new List<int> (m_Attributes.Keys);

        // EVENTS: --------------------------------------------------------------------------------

        public event Action<IDString> EventChange;

        // CONSTRUCTOR: ---------------------------------------------------------------------------

        internal RuntimeAttributes (Traits traits)
        {
            m_Traits = traits;
            m_Attributes = new Dictionary<int, RuntimeAttributeData> ();
        }

        internal RuntimeAttributes (Traits traits, Dictionary<IDString, OverrideAttributeData> overrideAttributes) :
            this (traits)
        {
            for (int i = 0; i < m_Traits.TraitClass.AttributesLength; ++i)
            {
                Attribute attribute = m_Traits.TraitClass.GetAttribute (i);
                if (attribute == null || attribute == null)
                {
                    const string error = "No Attribute reference found";
                    throw new NullReferenceException (error);
                }

                IDString attributeID = attribute.ID;
                if (m_Attributes.ContainsKey (attributeID.Hash))
                {
                    string error = $"Duplicate Attribute '{attributeID.String}' has already been defined";
                    throw new Exception (error);
                }

                RuntimeAttributeData data = new RuntimeAttributeData (m_Traits, attribute);
                if (!attribute.IsHidden &&
                    overrideAttributes.TryGetValue (attributeID, out OverrideAttributeData overrideData))
                {
                    if (overrideData.ChangeStartPercent)
                    {
                        data.Value = MathUtils.Lerp (
                            data.MinValue,
                            data.MaxValue,
                            overrideData.StartPercent
                        );
                    }
                }

                data.EventChange += ExecuteEventChange;
                m_Attributes[attributeID.Hash] = data;
            }
        }

        // PRIVATE METHODS: -----------------------------------------------------------------------

        private void ExecuteEventChange (IDString attributeID, double change)
        {
            LastChange = change;
            EventChange?.Invoke (attributeID);
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        /// <summary>
        /// Returns the RuntimeAttributeData instance of a requested attribute. Throws an
        /// exception if the requested attribute cannot be found.
        /// </summary>
        /// <param name="attributeID"></param>
        /// <returns></returns>
        public RuntimeAttributeData Get (IDString attributeID)
        {
            if (m_Attributes == null) return null;
            if (m_Attributes.TryGetValue (attributeID.Hash, out RuntimeAttributeData attribute))
            {
                return attribute;
            }

            // string objectName = this.m_Traits.gameObject.name;
            throw new Exception ($"Cannot find Attribute '{attributeID.String}'"); // in {objectName}");
        }

        /// <summary>
        /// Returns the RuntimeAttributeData instance of a requested attribute. Throws an
        /// exception if the requested attribute cannot be found.
        /// </summary>
        /// <param name="attributeID"></param>
        /// <returns></returns>
        public RuntimeAttributeData Get (string attributeID)
        {
            return Get (new IDString (attributeID));
        }

        // INTERNAL METHODS: ----------------------------------------------------------------------

        /// <summary>
        /// Returns the RuntimeAttributeData instance of a requested attribute.
        /// </summary>
        /// <param name="attrHash"></param>
        /// <returns></returns>
        public RuntimeAttributeData Get (int attrHash)
        {
            return m_Attributes.TryGetValue (attrHash, out RuntimeAttributeData stat)
                ? stat
                : null;
        }
    }
}