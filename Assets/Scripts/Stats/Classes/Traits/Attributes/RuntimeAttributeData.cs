using System;

namespace Stats
{
    public class RuntimeAttributeData
    {
        private readonly Traits m_Traits;
        private readonly Attribute m_Attribute;

        private readonly double m_MinValue;
        private readonly Stat m_MaxValue;
        private double m_Value;

        // PROPERTIES: ----------------------------------------------------------------------------

        public IDString ID => m_Attribute.ID;
        
        public double MinValue => m_MinValue;
        public double MaxValue => m_MaxValue != null
            ? m_Traits.RuntimeStats.Get (m_MaxValue.ID).Value
            : 0f;

        public double Value
        {
            get => m_Value;
            set
            {
                double oldValue = Value;
                double newValue = MathUtils.Clamp (value, MinValue, MaxValue);
                if (System.Math.Abs (m_Value - newValue) < float.Epsilon) return;

                m_Value = newValue;
                EventChange?.Invoke (m_Attribute.ID, newValue - oldValue);
            }
        }

        public double Ratio => (Value - MinValue) / (MaxValue - MinValue);

        // EVENTS: --------------------------------------------------------------------------------

        public event Action<IDString, double> EventChange;

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public RuntimeAttributeData (Traits traits, Attribute attribute)
        {
            m_Traits = traits;
            m_Attribute = attribute;

            m_MinValue = attribute.MinValue;
            m_MaxValue = attribute.MaxValue;

            m_Value = MathUtils.Lerp (MinValue, MaxValue, attribute.StartPercent);

            if (m_MaxValue != null)
            {
                traits.RuntimeStats.EventChange += RecalculateValue;
            }
        }

        // PRIVATE METHODS: -----------------------------------------------------------------------

        private void RecalculateValue (IDString statID)
        {
            double value = MathUtils.Clamp (m_Value, MinValue, MaxValue);
            if (System.Math.Abs (Value - value) < float.Epsilon) return;

            Value = m_Value;
        }
    }
}