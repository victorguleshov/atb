using System.Runtime.Serialization;
using ProtoBuf;

namespace Stats
{
    [DataContract]
    [ProtoContract]
    public class OverrideAttributeData
    {
        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)]
        private bool m_ChangeStartPercent = false;

        [ProtoMember (2)] [DataMember (EmitDefaultValue = false)]
        private double m_StartPercent = 1;

        // PROPERTIES: ----------------------------------------------------------------------------

        public bool ChangeStartPercent => m_ChangeStartPercent;
        public double StartPercent => m_StartPercent;

        public OverrideAttributeData () { }
    }
}