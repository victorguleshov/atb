using System.Collections.Generic;

namespace Stats
{
    public static class OverrideAttributesExtensions
    {
        public static Dictionary<IDString, OverrideAttributeData> SyncWithClass (
            this Dictionary<IDString, OverrideAttributeData> overrideAttributes, TraitClass traitTraitClass)
        {
            Dictionary<IDString, OverrideAttributeData> data =
                new Dictionary<IDString, OverrideAttributeData> ();

            for (int i = 0; i < traitTraitClass.AttributesLength; ++i)
            {
                Attribute attribute = traitTraitClass.GetAttribute (i);
                if (attribute == null || attribute.IsHidden) continue;

                overrideAttributes.TryGetValue (attribute.ID, out OverrideAttributeData entry);
                data[attribute.ID] = entry ?? new OverrideAttributeData ();
            }

            return data;
        }
    }
}