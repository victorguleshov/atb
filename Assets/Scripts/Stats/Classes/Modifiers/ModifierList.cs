using System.Collections.Generic;

namespace Stats
{
    internal class ModifierList
    {
        private readonly int statID;
        private readonly List<Modifier> list;

        // PROPERTIES: ----------------------------------------------------------------------------

        public double Value { get; private set; }

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public ModifierList (int statID)
        {
            this.statID = statID;
            list = new List<Modifier> ();

            Value = 0f;
        }

        public ModifierList (int statID, List<Modifier> list) : this (statID)
        {
            foreach (Modifier element in list)
            {
                Value += element.Value;
                this.list.Add (element.Clone);
            }
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public void Add (double value)
        {
            Value += value;
            list.Add (new Modifier (statID, value));
        }

        public bool Remove (double value)
        {
            for (int i = list.Count - 1; i >= 0; --i)
            {
                if (System.Math.Abs (list[i].Value - value) < float.Epsilon)
                {
                    Value -= list[i].Value;
                    list.RemoveAt (i);

                    return true;
                }
            }

            return false;
        }
    }
}