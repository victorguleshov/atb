using System.Runtime.Serialization;
using ProtoBuf;

namespace Stats
{
    [DataContract]
    [ProtoContract]
    public struct Modifier
    {
        // MEMBERS: -------------------------------------------------------------------------------

        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)]
        private int statID;

        [ProtoMember (2)] [DataMember (EmitDefaultValue = false)]
        private double value;

        // PROPERTIES: ----------------------------------------------------------------------------

        public int StatID => statID;
        public double Value => value;

        public Modifier Clone => new Modifier (statID, value);

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public Modifier (int statID, double value)
        {
            this.statID = statID;
            this.value = value;
        }
    }
}