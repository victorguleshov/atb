namespace Stats
{
    public enum ModifierType
    {
        Constant = 0,
        Percent = 1
    }
}