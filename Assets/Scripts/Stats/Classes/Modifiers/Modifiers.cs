using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Stats
{
    [DataContract]
    internal class Modifiers
    {
        private ModifierList percentages;
        private ModifierList constants;

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public Modifiers (int statID)
        {
            percentages = new ModifierList (statID);
            constants = new ModifierList (statID);
        }

        public Modifiers (int statID, List<Modifier> percentages, List<Modifier> constants)
        {
            this.percentages = new ModifierList (statID, percentages);
            this.constants = new ModifierList (statID, constants);
        }

        public Modifiers () { }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public double Calculate (double value)
        {
            value += constants.Value;
            value *= 1f + percentages.Value;

            return value;
        }

        // INTERNAL METHODS: ----------------------------------------------------------------------

        internal void AddPercentage (double percent)
        {
            percentages.Add (percent);
        }

        internal void AddConstant (double value)
        {
            constants.Add (value);
        }

        internal bool RemovePercentage (double percent)
        {
            return percentages.Remove (percent);
        }

        internal bool RemoveConstant (double value)
        {
            return constants.Remove (value);
        }
    }
}