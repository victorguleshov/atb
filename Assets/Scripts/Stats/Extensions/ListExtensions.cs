using System;
using System.Collections.Generic;

namespace Stats
{
    public static class ListExtensions
    {
        public static bool TryGetFirst<T> (this IEnumerable<T> source, Predicate<T> match, out T found)
        {
            found = default;
            foreach (var item in source)
            {
                if (match.Invoke (item))
                {
                    found = item;
                    return true;
                }
            }
            return false;
        }

        private static Random random;

        /// <summary>Shuffles the list</summary>
        public static IList<T> Shuffle<T> (this IList<T> list, Random random = null)
        {
            if (random == null)
            {
                ListExtensions.random ??= new Random ();
                random = ListExtensions.random;
            }

            var count = list.Count;
            while (count > 1)
            {
                --count;
                var index = random.Next (count + 1);
                (list[index], list[count]) = (list[count], list[index]);
            }

            return list;
        }
    }
}