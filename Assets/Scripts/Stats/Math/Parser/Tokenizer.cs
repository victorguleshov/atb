using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace Stats.Math.Parser
{
    public class Tokenizer
    {
        public enum TokenType
        {
            EndOfExpression,
            Add,
            Subtract,
            Multiply,
            Divide,
            OpenParenthesis,
            CloseParenthesis,
            Number,
        }

        // CONSTANTS: -----------------------------------------------------------------------------

        private static readonly StringBuilder StringBuilder = new StringBuilder ();
        private static readonly CultureInfo Culture = CultureInfo.InvariantCulture;

        private const char CHAR_EOE = '\0';
        private const char CHAR_ADD = '+';
        private const char CHAR_SUBTRACT = '-';
        private const char CHAR_MULTIPLY = '*';
        private const char CHAR_DIVIDE = '/';
        private const char CHAR_PARENTHESIS_OPEN = '(';
        private const char CHAR_PARENTHESIS_CLOSE = ')';
        private const char CHAR_DOT = '.';

        // MEMBERS: -------------------------------------------------------------------------------

        private readonly TextReader m_Reader;

        // PROPERTIES: ----------------------------------------------------------------------------

        private char CurrentCharacter { get; set; }

        public TokenType Type { get; private set; }
        public float Number { get; private set; }

        // INITIALIZER: ---------------------------------------------------------------------------

        public Tokenizer (string expression)
        {
            m_Reader = new StringReader (expression);

            NextCharacter ();
            NextToken ();
        }

        // PRIVATE METHODS: -----------------------------------------------------------------------

        private void NextCharacter ()
        {
            int character = m_Reader.Read ();
            CurrentCharacter = character < 0 ? CHAR_EOE : (char) character;
        }

        public void NextToken ()
        {
            while (char.IsWhiteSpace (CurrentCharacter)) NextCharacter ();
            switch (CurrentCharacter)
            {
                case CHAR_EOE:
                    Type = TokenType.EndOfExpression;
                    return;

                case CHAR_ADD:
                    NextCharacter ();
                    Type = TokenType.Add;
                    return;

                case CHAR_SUBTRACT:
                    NextCharacter ();
                    Type = TokenType.Subtract;
                    return;

                case CHAR_MULTIPLY:
                    NextCharacter ();
                    Type = TokenType.Multiply;
                    return;

                case CHAR_DIVIDE:
                    NextCharacter ();
                    Type = TokenType.Divide;
                    return;

                case CHAR_PARENTHESIS_OPEN:
                    NextCharacter ();
                    Type = TokenType.OpenParenthesis;
                    return;

                case CHAR_PARENTHESIS_CLOSE:
                    NextCharacter ();
                    Type = TokenType.CloseParenthesis;
                    return;
            }

            if (!char.IsDigit (CurrentCharacter) && CurrentCharacter != CHAR_DOT)
            {
                throw new Exception ($"Unexpected character: {CurrentCharacter}");
            }

            StringBuilder.Clear ();
            bool hasFloatingPoint = false;

            while (char.IsDigit (CurrentCharacter) ||
                   !hasFloatingPoint && CurrentCharacter == CHAR_DOT)
            {
                StringBuilder.Append (CurrentCharacter);
                hasFloatingPoint = CurrentCharacter == CHAR_DOT;
                NextCharacter ();
            }

            Number = float.Parse (StringBuilder.ToString (), Culture);
            Type = TokenType.Number;
        }
    }
}