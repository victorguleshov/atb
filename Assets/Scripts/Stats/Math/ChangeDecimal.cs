using System;
using System.Runtime.Serialization;
using ProtoBuf;

namespace Stats.Math
{
    [DataContract]
    [ProtoContract]
    public class ChangeDecimal
    {
        public enum Operation
        {
            Set,
            Add,
            Subtract,
            Multiply,
            Divide,
        }

        // MEMBERS: -------------------------------------------------------------------------------

        [ProtoMember (1)] [DataMember (EmitDefaultValue = false)] private Operation operation;
        [ProtoMember (2)] [DataMember (EmitDefaultValue = false)] private double value;

        // PROPERTIES: ----------------------------------------------------------------------------

        public string OperationName => operation.ToString ();

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public ChangeDecimal ()
        {
            operation = Operation.Set;
            value = 0f;
        }

        public ChangeDecimal (double value) : this ()
        {
            this.value = value;
        }

        public ChangeDecimal (float value) : this ()
        {
            this.value = value;
        }

        public ChangeDecimal (Operation operation, double value)
        {
            this.operation = operation;
            this.value = value;
        }

        public ChangeDecimal (Operation operation, float value)
        {
            this.operation = operation;
            this.value = value;
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public double Get (double value)
        {
            return operation switch
            {
                Operation.Set => this.value,
                Operation.Add => value + this.value,
                Operation.Subtract => value - this.value,
                Operation.Multiply => value * this.value,
                Operation.Divide => value / this.value,
                _ => throw new ArgumentOutOfRangeException ($"Unknown operation {operation}")
            };
        }

        // OVERRIDES: -----------------------------------------------------------------------------

        public override string ToString ()
        {
            return operation switch
            {
                Operation.Set => $"= {value}",
                Operation.Add => $"+ {value}",
                Operation.Subtract => $"- {value}",
                Operation.Multiply => $"* {value}",
                Operation.Divide => $"/ {value}",
                _ => throw new ArgumentOutOfRangeException ()
            };
        }
    }
}