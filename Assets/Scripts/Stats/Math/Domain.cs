namespace Stats.Math
{
    public class Domain
    {
        // PROPERTIES: ----------------------------------------------------------------------------

        public Table Table { get; }
        public Traits Source { get; }
        public Traits Target { get; }
        public KeyValueArgs[] Args { get; }

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public Domain (Traits source, Traits target, Table table, params KeyValueArgs[] args)
        {
            Table = table;
            Source = source;
            Target = target;
            Args = args;
        }
    }
}