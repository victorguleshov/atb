using System.Text;

namespace Stats.Math
{
    public class Expression
    {
        // PROPERTIES: ----------------------------------------------------------------------------

        public StringBuilder Term { get; }
        public char Operation { get; set; }
        public bool NeedsEvaluation { get; set; }

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public Expression ()
        {
            Term = new StringBuilder ();
            NeedsEvaluation = false;
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public bool HasOperation ()
        {
            return Operation != default;
        }
    }
}