﻿namespace Stats.Math.Symbols
{
    internal class SymbolNumber : ISymbol
    {
        private readonly float m_Number;

        // CONSTRUCTOR: ---------------------------------------------------------------------------

        public SymbolNumber (float number)
        {
            m_Number = number;
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public float Evaluate ()
        {
            return m_Number;
        }
    }
}