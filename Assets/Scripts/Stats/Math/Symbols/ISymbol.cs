﻿namespace Stats.Math.Symbols
{
    internal interface ISymbol
    {
        float Evaluate ();
    }
}