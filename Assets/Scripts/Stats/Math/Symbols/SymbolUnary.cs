﻿namespace Stats.Math.Symbols
{
    internal class SymbolUnary : ISymbol
    {
        private readonly ISymbol m_RHS;
        private readonly Parser.Parser.UnaryOperation m_Operation;

        // CONSTRUCTOR: ---------------------------------------------------------------------------

        public SymbolUnary (ISymbol rhs, Parser.Parser.UnaryOperation operation)
        {
            m_RHS = rhs;
            m_Operation = operation;
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public float Evaluate ()
        {
            float value = m_RHS.Evaluate ();
            return m_Operation (value);
        }
    }
}