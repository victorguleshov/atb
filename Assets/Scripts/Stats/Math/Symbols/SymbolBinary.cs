﻿namespace Stats.Math.Symbols
{
    internal class SymbolBinary : ISymbol
    {
        private readonly ISymbol m_LHS;
        private readonly ISymbol m_RHS;

        private readonly Parser.Parser.BinaryOperation m_Operation;

        // CONSTRUCTOR: ---------------------------------------------------------------------------

        public SymbolBinary (ISymbol lhs, ISymbol rhs, Parser.Parser.BinaryOperation operation)
        {
            m_LHS = lhs;
            m_RHS = rhs;

            m_Operation = operation;
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public float Evaluate ()
        {
            float valueA = m_LHS.Evaluate ();
            float valueB = m_RHS.Evaluate ();

            return m_Operation (valueA, valueB);
        }
    }
}