using System;
using System.Text.RegularExpressions;

namespace Stats.Math
{
    public class Operation
    {
        // MEMBERS: -------------------------------------------------------------------------------

        private readonly Func<Domain, string, string> m_Function;
        private readonly Regex m_Regex;

        // CONSTRUCTORS: --------------------------------------------------------------------------

        public Operation (string pattern, Func<Domain, string, string> function)
        {
            m_Regex = new Regex (pattern);
            m_Function = function;
        }

        // PUBLIC METHODS: ------------------------------------------------------------------------

        public Match Match (string input)
        {
            return m_Regex.Match (input);
        }

        public string Run (Domain data, string clause)
        {
            return m_Function.Invoke (data, clause);
        }
    }
}