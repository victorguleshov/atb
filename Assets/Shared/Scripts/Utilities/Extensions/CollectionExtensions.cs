using System;
using System.Collections.Generic;
using System.Linq;

namespace Extensions
{
    public static class CollectionExtensions
    {
        public static int IndexOf<T> (this IEnumerable<T> colection, T value)
        {
            if (colection == null) throw new ArgumentNullException (nameof (colection));

            var index = 0;
            foreach (var item in colection)
            {
                if (Equals (item, value)) return index;
                index++;
            }

            return -1;
        }

        public static int IndexOf<T> (this IEnumerable<T> colection, Predicate<T> match)
        {
            if (colection == null) throw new ArgumentNullException (nameof (colection));

            var index = 0;
            foreach (var item in colection)
            {
                if (match (item)) return index;
                index++;
            }

            return -1;
        }

        public static T Find<T> (this IEnumerable<T> colection, Predicate<T> match)
        {
            if (colection == null) throw new ArgumentNullException (nameof (colection));

            foreach (var item in colection)
                if (match (item))
                    return item;

            return default;
        }
        public static void PushRange<T> (this Stack<T> stack, IEnumerable<T> range)
        {
            var collection = range.ToList ();
            foreach (var item in collection) stack.Push (item);
        }
    }
}