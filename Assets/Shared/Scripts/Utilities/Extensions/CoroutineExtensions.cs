﻿using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

namespace Utilities.Extensions
{
    [SuppressMessage ("ReSharper", "UnusedMember.Global")]
    [SuppressMessage ("ReSharper", "UnusedMethodReturnValue.Global")]
    public static class CoroutineExtensions
    {
        public static Coroutine WaitForSeconds<TMono> (this TMono p_owner, float p_seconds, Action p_onEnd) where TMono : MonoBehaviour
        {
            if (p_owner && p_owner.isActiveAndEnabled)
                return p_owner.StartCoroutine (IE_WaitForSeconds (p_seconds, p_onEnd));

            p_onEnd?.Invoke ();
            return null;
        }

        public static Coroutine WaitForSecondsRealtime<TMono> (this TMono p_owner, float p_seconds, Action p_onEnd) where TMono : MonoBehaviour
        {
            if (p_owner && p_owner.isActiveAndEnabled)
                return p_owner.StartCoroutine (IE_WaitForSecondsRealtime (p_seconds, p_onEnd));

            p_onEnd?.Invoke ();
            return null;
        }

        public static Coroutine WaitForFrames<TMono> (this TMono p_owner, int p_frames, Action p_onEnd) where TMono : MonoBehaviour
        {
            if (p_owner && p_owner.isActiveAndEnabled)
                return p_owner.StartCoroutine (IE_WaitForFrames (p_frames, p_onEnd));

            p_onEnd?.Invoke ();
            return null;
        }

        public static Coroutine WaitForFixedFrames<TMono> (this TMono p_owner, int p_frames, Action p_onEnd) where TMono : MonoBehaviour
        {
            if (p_owner && p_owner.isActiveAndEnabled)
                return p_owner.StartCoroutine (IE_WaitForFixedFrames (p_frames, p_onEnd));

            p_onEnd?.Invoke ();
            return null;
        }

        public static Coroutine WaitWhile<TMono> (this TMono p_owner, Func<bool> p_predicate, Action p_onEnd) where TMono : MonoBehaviour
        {
            if (p_owner && p_owner.isActiveAndEnabled)
                return p_owner.StartCoroutine (IE_WaitWhile (p_predicate, p_onEnd));

            p_onEnd?.Invoke ();
            return null;
        }

        public static Coroutine WaitUntil<TMono> (this TMono p_owner, Func<bool> p_predicate, Action p_onEnd) where TMono : MonoBehaviour
        {
            if (p_owner && p_owner.isActiveAndEnabled)
                return p_owner.StartCoroutine (IE_WaitUntil (p_predicate, p_onEnd));

            p_onEnd?.Invoke ();
            return null;
        }

        public static void StopWaiting<TMono> (this TMono p_owner, ref Coroutine ref_routine) where TMono : MonoBehaviour
        {
            if (p_owner && ref_routine != null)
            {
                p_owner.StopCoroutine (ref_routine);
                ref_routine = null;
            }
        }

        private static IEnumerator IE_WaitForSeconds (float p_seconds, Action p_onEnd)
        {
            if (p_seconds > 0.0f)
                yield return new WaitForSeconds (p_seconds);

            p_onEnd?.Invoke ();
        }

        private static IEnumerator IE_WaitForSecondsRealtime (float p_seconds, Action p_onEnd)
        {
            if (p_seconds > 0.0f)
                yield return new WaitForSecondsRealtime (p_seconds);

            p_onEnd?.Invoke ();
        }

        private static IEnumerator IE_WaitForFrames (int p_frames, Action p_onEnd)
        {
            if (p_frames > 0)
            {
                var l_waitForEndOfFrame = new WaitForEndOfFrame ();

                while (p_frames > 0)
                {
                    p_frames--;
                    yield return l_waitForEndOfFrame;
                }
            }

            p_onEnd?.Invoke ();
        }

        private static IEnumerator IE_WaitForFixedFrames (int p_frames, Action p_onEnd)
        {
            if (p_frames > 0)
            {
                var l_waitForFixedUpdate = new WaitForFixedUpdate ();

                while (p_frames > 0)
                {
                    p_frames--;
                    yield return l_waitForFixedUpdate;
                }
            }

            p_onEnd?.Invoke ();
        }

        private static IEnumerator IE_WaitWhile (Func<bool> p_predicate, Action p_onEnd)
        {
            if (p_predicate?.Invoke () ?? false)
                yield return new WaitWhile (p_predicate);

            p_onEnd?.Invoke ();
        }

        private static IEnumerator IE_WaitUntil (Func<bool> p_predicate, Action p_onEnd)
        {
            if ((p_predicate?.Invoke () ?? true) == false)
                yield return new WaitUntil (p_predicate);

            p_onEnd?.Invoke ();
        }
    }
}