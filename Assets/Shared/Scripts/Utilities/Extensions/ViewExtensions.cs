using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Utilities.Extensions
{
    public class ViewExtensions
    {
        /// <param name="views">Список вьюшек</param>
        /// <param name="datas">Список данных</param>
        /// <param name="action">Че куда вставлять</param>
        public static void SetViews<T0, T1> (List<T0> views, List<T1> datas, Func<T0, T1, int, bool> func) where T0 : Component
        {
            if (views.Count == 0) return;
            var maxSiblingIndexDelta = views[0].transform.parent.childCount - views.Max (x => x.transform.GetSiblingIndex ());
            var datasIndex = 0;
            var viewsIndex = 0;
            while (viewsIndex < views.Count)
            {
                var view = views[viewsIndex];

                if (viewsIndex < datas.Count && datasIndex < datas.Count)
                {
                    var data = datas[datasIndex];

                    view.gameObject.SetActive (true);

                    if (!func (view, data, datasIndex)) viewsIndex--;
                }
                else
                {
                    view.gameObject.SetActive (false);
                }

                viewsIndex++;
                datasIndex++;
            }

            T0 newView = null;
            var savedViewsCount = views.Count;
            while (datasIndex < datas.Count)
            {
                if (newView == null)
                {
                    newView = GameObject.Instantiate (views[datasIndex % savedViewsCount], views[datasIndex % savedViewsCount].transform.parent);
                    newView.transform.SetSiblingIndex (newView.transform.parent.childCount - maxSiblingIndexDelta);
                    views.Add (newView);
                }

                var data = datas[datasIndex];

                newView.gameObject.SetActive (true);
                if (func (newView, data, datasIndex)) newView = null;

                datasIndex++;
            }

            if (newView != null) newView.gameObject.SetActive (false);
        }

        /// <param name="views">Список вьюшек</param>
        /// <param name="datas">Список данных</param>
        /// <param name="action">Че куда вставлять</param>
        public static void SetViews<T0> (List<T0> views, int count, Func<T0, int, bool> func) where T0 : Component
        {
            if (views.Count == 0) return;
            var maxSiblingIndexDelta = views[0].transform.parent.childCount - views.Max (x => x.transform.GetSiblingIndex ());
            var datasIndex = 0;
            var viewsIndex = 0;
            while (viewsIndex < views.Count)
            {
                var view = views[viewsIndex];

                if (viewsIndex < count && datasIndex < count)
                {
                    view.gameObject.SetActive (true);

                    if (!func (view, datasIndex)) viewsIndex--;
                }
                else
                {
                    view.gameObject.SetActive (false);
                }

                viewsIndex++;
                datasIndex++;
            }

            T0 newView = null;
            var savedViewsCount = views.Count;
            while (datasIndex < count)
            {
                if (newView == null)
                {
                    newView = GameObject.Instantiate (views[datasIndex % savedViewsCount], views[datasIndex % savedViewsCount].transform.parent);
                    newView.transform.SetSiblingIndex (newView.transform.parent.childCount - maxSiblingIndexDelta);
                    views.Add (newView);
                }

                newView.gameObject.SetActive (true);
                if (func (newView, datasIndex)) newView = null;

                datasIndex++;
            }

            if (newView != null) newView.gameObject.SetActive (false);
        }

        public static void AddViews<T0> (List<T0> views, int count, Action<T0> action = null) where T0 : Component
        {
            var data = new List<int> ();
            for (var i = 0; i < count; i++) data.Add (i);

            AddViews (views, data, action: (view, _, __) => action?.Invoke (view));
        }

        public static void AddViews<T0, T1> (
            List<T0> views, List<T1> datas,
            Func<T0, T1, int, bool> func = null,
            Action<T0, T1, int> action = null
        ) where T0 : Component
        {
            var datasIndex = 0;

            T0 newView = null;
            var savedViewsCount = views.Count;
            while (datasIndex < datas.Count)
            {
                if (newView == null)
                {
                    newView = GameObject.Instantiate (views[datasIndex % savedViewsCount], views[datasIndex % savedViewsCount].transform.parent);
                    views.Add (newView);
                }

                var data = datas[datasIndex];

                newView.gameObject.SetActive (true);

                if (func != null)
                    if (func.Invoke (newView, data, datasIndex))
                        newView = null;

                if (action != null)
                {
                    action.Invoke (newView, data, datasIndex);
                    newView = null;
                }

                datasIndex++;
            }

            if (newView != null) newView.gameObject.SetActive (false);
        }

        public static void RemoveViews<T0> (List<T0> views, int count) where T0 : Component
        {
            var datasIndex = 0;

            while (datasIndex < count)
            {
                var view = views[views.Count - 1];
                GameObject.Destroy (view.gameObject);
                views.RemoveAt (views.Count - 1);

                datasIndex++;
            }
        }
    }
}