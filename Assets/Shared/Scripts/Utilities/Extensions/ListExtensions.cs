using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

namespace Utilities.Extensions
{
    public static class ListExtensions
    {
        public static IList<T> Shuffle<T> (this IList<T> list)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = UnityEngine.Random.Range (0, n + 1);
                (list[k], list[n]) = (list[n], list[k]);
            }
            return list;
        }
        public static IList<T> Shuffle<T> (this IList<T> list, int seed)
        {
            return list.Shuffle (new Random (seed));
        }
        public static IList<T> Shuffle<T> (this IList<T> list, Random random)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = random.Next (0, n + 1);
                (list[k], list[n]) = (list[n], list[k]);
            }
            return list;
        }

        public static bool ContainsAll<T> (this IList<T> list, T[] candidate)
        {
            if (IsEmptyLocate (list, candidate))
                return false;

            if (candidate.Length > list.Count)
                return false;

            for (var a = 0; a <= list.Count - candidate.Length; a++)
                if (list[a].Equals (candidate[0]))
                {
                    var i = 0;
                    for (; i < candidate.Length; i++)
                        if (false == list[a + i].Equals (candidate[i]))
                            break;

                    if (i == candidate.Length)
                        return true;
                }

            return false;
        }

        public static bool ContainsAll<T> (this IList<T> list, IList<T> candidate)
        {
            if (IsEmptyLocate (list, candidate))
                return false;

            if (candidate.Count > list.Count)
                return false;

            for (var a = 0; a <= list.Count - candidate.Count; a++)
                if (list[a].Equals (candidate[0]))
                {
                    var i = 0;
                    for (; i < candidate.Count; i++)
                        if (false == list[a + i].Equals (candidate[i]))
                            break;

                    if (i == candidate.Count)
                        return true;
                }

            return false;
        }

        public static T RandomValue<T> (this IEnumerable<T> collection)
        {
            var array = collection as T[] ?? collection.ToArray ();
            return array.Length > 0 ? array.ElementAt (UnityEngine.Random.Range (0, array.Length)) : default;
        }

        private static bool IsEmptyLocate<T> (this ICollection<T> list, ICollection<T> candidate)
        {
            return list == null
                   || candidate == null
                   || list.Count == 0
                   || candidate.Count == 0
                   || candidate.Count > list.Count;
        }

        public static IEnumerable<T> Clone<T> (this IEnumerable<T> p_original) where T : ICloneable
        {
            IEnumerable<T> r_result = default;

            if (p_original != null)
            {
                var l_originalAsArray = p_original as T[] ?? p_original.ToArray ();

                if (l_originalAsArray.Length > 0)
                {
                    r_result = new T[l_originalAsArray.Length];

                    for (var i = 0; i < l_originalAsArray.Length; i++) ((T[]) r_result)[i] = (T) l_originalAsArray[i]?.Clone ();
                }
            }

            return r_result;
        }

        /// <summary>
        ///     "Нечестный рандом", учитывающий "вес" каждого элемента;<br />
        ///     Возвращает index элемента из <see cref="p_chances" />;
        /// </summary>
        public static int DishonestRandom (this IEnumerable<float> p_chances)
        {
            if (p_chances != null)
            {
                var l_chances = p_chances as float[] ?? p_chances.ToArray ();

                var l_total = l_chances.Sum (i_chance => Mathf.Max (0.0f, i_chance));

                var l_randomValue = UnityEngine.Random.Range (0.0f, l_total);
                var l_checkedValue = 0.0f;

                for (var i_index = 0; i_index < l_chances.Length; i_index++)
                {
                    var i_chance = Mathf.Max (0.0f, l_chances[i_index]);

                    if (i_chance > 0.0f)
                    {
                        if (l_randomValue >= l_checkedValue &&
                            l_randomValue <= l_checkedValue + i_chance)
                            return i_index;

                        l_checkedValue += i_chance;
                    }
                }
            }

            return 0;
        }

        public static int DishonestRandom (this Random random, IEnumerable<float> p_chances)
        {
            if (p_chances != null)
            {
                var l_chances = p_chances as float[] ?? p_chances.ToArray ();

                var l_total = l_chances.Sum (i_chance => Mathf.Max (0.0f, i_chance));

                var l_randomValue = random.NextDouble (0.0f, l_total);
                var l_checkedValue = 0.0f;

                for (var i_index = 0; i_index < l_chances.Length; i_index++)
                {
                    var i_chance = Mathf.Max (0.0f, l_chances[i_index]);

                    if (i_chance > 0.0f)
                    {
                        if (l_randomValue >= l_checkedValue &&
                            l_randomValue <= l_checkedValue + i_chance)
                            return i_index;

                        l_checkedValue += i_chance;
                    }
                }
            }

            return 0;
        }

        public static int Choose (this List<float> probs, out float choseValue)
        {
            var total = probs.Sum ();

            var rnd = UnityEngine.Random.value;

            var randomPoint = rnd * total;

            for (var i = 0; i < probs.Count; i++)
            {
                if (randomPoint < probs[i])
                {
                    choseValue = probs[i];
                    return i;
                }
                randomPoint -= probs[i];
            }

            choseValue = probs[probs.Count - 1];
            return probs.Count - 1;
        }

        public static double NextDouble (this Random random, double minValue, double maxValue)
        {
            return random.NextDouble () * (minValue - maxValue) + minValue;
        }

        public static double RestrictWithinLength (double value, double length)
        {
            if (value < 0.0f || value >= length)
            {
                var remainderAbs = Math.Abs (value % length);
                value = value < 0.0f ? length - remainderAbs : remainderAbs;

                if (Math.Abs (value - length) < 0.0001f)
                    value = 0.0f;
            }

            return value;
        }
    }
}