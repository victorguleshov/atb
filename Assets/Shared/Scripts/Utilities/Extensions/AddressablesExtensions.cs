using System;
using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace ATB
{
    public static class AddressablesExtensions
    {
        public static async UniTaskVoid LoadAssetAsync<T> (string id, Action<AsyncOperationHandle<T>> onReady)
        {
            var handler = Addressables.LoadAssetAsync<T> (id);

            while (handler.IsDone == false) await UniTask.DelayFrame (1);

            onReady?.Invoke (handler);
        }

        public static async UniTaskVoid LoadAssetsAsync<T> (string[] ids, Action<AsyncOperationHandle<T>[]> onReady)
        {
            var handlers = new AsyncOperationHandle<T> [ids.Length];

            for (var i = 0; i < handlers.Length; i++) handlers[i] = Addressables.LoadAssetAsync<T> (i);

            for (var i = 0; i < handlers.Length;)
                if (handlers[i].IsDone)
                    i++;
                else
                    await UniTask.DelayFrame (1);

            onReady?.Invoke (handlers);
        }
    }
}