using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Utilities
{
    public class ObjectPool : MonoBehaviour
    {
        public GameObject Prefab;
        public int InitialSize;

        private readonly Stack<GameObject> instances = new Stack<GameObject> ();
        private readonly List<GameObject> objectsToReturn = new List<GameObject> ();

        private void Awake ()
        {
            Assert.IsNotNull (Prefab);

            if (Prefab.transform.parent == transform)
            {
                var pooledObject = Prefab.GetComponent<PooledObject> ();
                if (!pooledObject) pooledObject = Prefab.AddComponent<PooledObject> ();
                pooledObject.Pool = this;
                Prefab.SetActive (false);
                if (!instances.Contains (Prefab))
                    instances.Push (Prefab);
            }

            Initialize ();
        }

        public void Reset ()
        {
            objectsToReturn.Clear ();
            foreach (var instance in transform.GetComponentsInChildren<PooledObject> ())
                if (instance.gameObject.activeSelf)
                    objectsToReturn.Add (instance.gameObject);

            foreach (var instance in objectsToReturn)
                ReturnObject (instance);
        }

        public void Initialize ()
        {
            for (var i = instances.Count; i < InitialSize; i++)
            {
                var obj = CreateInstance ();
                obj.SetActive (false);
                instances.Push (obj);
            }
        }

        public GameObject GetObject ()
        {
            var obj = instances.Count > 0 ? instances.Pop () : CreateInstance ();
            obj.SetActive (true);
            return obj;
        }

        public void ReturnObject (GameObject obj)
        {
            var pooledObject = obj.GetComponent<PooledObject> ();
            Assert.IsNotNull (pooledObject);
            Assert.IsTrue (pooledObject.Pool == this);

            obj.SetActive (false);
            if (!instances.Contains (obj))
                instances.Push (obj);
        }

        private GameObject CreateInstance ()
        {
            var obj = Instantiate (Prefab, transform, true);
            var pooledObject = obj.GetComponent<PooledObject> ();
            if (!pooledObject) pooledObject = obj.AddComponent<PooledObject> ();
            pooledObject.Pool = this;
            return obj;
        }
    }

    public class PooledObject : MonoBehaviour
    {
        public ObjectPool Pool;
    }
}