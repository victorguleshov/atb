using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Utilities
{
    public class ObjectPool<T> where T : MonoBehaviour, IPooledObject<T>
    {
        private readonly Stack<T> instances = new Stack<T> ();

        private Func<T> instantiateFunc;
        private T prefab;
        private Transform root;

        public ObjectPool<T> SetInstantiateFunc (Func<T> instantiateFunc = null)
        {
            this.instantiateFunc = instantiateFunc;
            return this;
        }

        public ObjectPool<T> SetPrefab (T prefab, Transform root = null)
        {
            this.prefab = prefab;
            this.root = root;
            return this;
        }

        public ObjectPool<T> SetRoot (Transform root)
        {
            this.root = root;
            return this;
        }

        public ObjectPool<T> Initialize (int count = 0)
        {
            if (prefab.transform.parent)
            {
                var pooledObject = prefab.GetComponent<T> ();
                if (pooledObject != null) pooledObject = prefab.gameObject.AddComponent<T> ();
                pooledObject.Pool = this;
                prefab.gameObject.SetActive (false);
                if (!instances.Contains (prefab))
                    instances.Push (prefab);
            }

            for (var i = instances.Count; i < count; i++)
            {
                var obj = CreateInstance ();
                obj.gameObject.SetActive (false);
                instances.Push (obj);
            }

            return this;
        }

        public T GetObject ()
        {
            var obj = instances.Count > 0 ? instances.Pop () : CreateInstance ();
            obj.gameObject.SetActive (true);
            return obj;
        }

        public void ReturnObject (T obj)
        {
            Assert.IsNotNull (obj);
            Assert.IsTrue (obj.Pool == this);

            obj.gameObject.SetActive (false);
            if (!instances.Contains (obj))
                instances.Push (obj);

            obj.transform.SetParent (root ? root : prefab ? prefab.transform.parent : null);
        }

        private T CreateInstance ()
        {
            T obj;
            if (instantiateFunc == null)
            {
                obj = GameObject.Instantiate (prefab, root ? root : prefab ? prefab.transform.parent : null);
                obj.gameObject.SetActive (true);
            }
            else
            {
                obj = instantiateFunc?.Invoke ();
            }

            obj.Pool = this;
            return obj;
        }
    }

    public interface IPooledObject<T> where T : MonoBehaviour, IPooledObject<T>
    {
        ObjectPool<T> Pool { get; set; }
    }
}