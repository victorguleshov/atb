using System;
using UnityEditor;

namespace UnityEngine.UI.Extensions
{
    [CustomEditor (typeof (FlowLayoutGroupFitter), true)]
    [CanEditMultipleObjects]
    public class FlowLayoutGroupFitterEditor : Editor
    {
        private SerializedProperty m_MinHeight;
        private SerializedProperty m_MinWidth;

        protected virtual void OnEnable ()
        {
            m_MinWidth = serializedObject.FindProperty ("m_MinWidth");
            m_MinHeight = serializedObject.FindProperty ("m_MinHeight");
        }

        public override void OnInspectorGUI ()
        {
            serializedObject.Update ();

            LayoutElementField (m_MinWidth, rectTransform => rectTransform.childCount > 0 ? ((RectTransform) rectTransform.GetChild (0)).sizeDelta.x : 0);
            LayoutElementField (m_MinHeight, rectTransform => rectTransform.childCount > 0 ? ((RectTransform) rectTransform.GetChild (0)).sizeDelta.y : 0);

            serializedObject.ApplyModifiedProperties ();
        }

        private void LayoutElementField (SerializedProperty property, float defaultValue)
        {
            LayoutElementField (property, _ => defaultValue);
        }

        private void LayoutElementField (SerializedProperty property, Func<RectTransform, float> defaultValue)
        {
            var position = EditorGUILayout.GetControlRect ();

            // Label
            var label = EditorGUI.BeginProperty (position, null, property);

            // Rects
            var fieldPosition = EditorGUI.PrefixLabel (position, label);

            var toggleRect = fieldPosition;
            toggleRect.width = 16;

            var floatFieldRect = fieldPosition;
            floatFieldRect.xMin += 16;

            // Checkbox
            EditorGUI.BeginChangeCheck ();
            var enabled = EditorGUI.ToggleLeft (toggleRect, GUIContent.none, property.floatValue >= 0);
            if (EditorGUI.EndChangeCheck ())

                // This could be made better to set all of the targets to their initial width, but mimizing code change for now
                property.floatValue = enabled ? defaultValue ((target as FlowLayoutGroupFitter).transform as RectTransform) : -1;

            if (!property.hasMultipleDifferentValues && property.floatValue >= 0)
            {
                // Float field
                EditorGUIUtility.labelWidth = 4; // Small invisible label area for drag zone functionality
                EditorGUI.BeginChangeCheck ();
                var newValue = EditorGUI.FloatField (floatFieldRect, new GUIContent (" "), property.floatValue);
                if (EditorGUI.EndChangeCheck ()) property.floatValue = Mathf.Max (0, newValue);
                EditorGUIUtility.labelWidth = 0;
            }

            EditorGUI.EndProperty ();
        }
    }
}