﻿using System;
using UnityEngine.EventSystems;

namespace UnityEngine.UI.Extensions
{
    [AddComponentMenu ("Layout/Extensions/Flow Layout Group Fitter")]
    [ExecuteAlways]
    [RequireComponent (typeof (RectTransform))]
    [RequireComponent (typeof (FlowLayoutGroup))]
    public class FlowLayoutGroupFitter : UIBehaviour, ILayoutSelfController
    {
        [SerializeField] private float m_MinWidth = -1;
        [SerializeField] private float m_MinHeight = -1;

        [NonSerialized] private RectTransform m_Rect;

        private DrivenRectTransformTracker m_Tracker;

        private RectTransform rectTransform
        {
            get
            {
                if (m_Rect == null)
                    m_Rect = GetComponent<RectTransform> ();
                return m_Rect;
            }
        }

        protected override void OnEnable ()
        {
            base.OnEnable ();
            SetDirty ();
        }

        protected override void OnDisable ()
        {
            m_Tracker.Clear ();
            LayoutRebuilder.MarkLayoutForRebuild (rectTransform);
            base.OnDisable ();
        }

        protected override void OnRectTransformDimensionsChange ()
        {
            SetDirty ();
        }

#if UNITY_EDITOR
        protected override void OnValidate ()
        {
            SetDirty ();
        }

#endif

        /// <summary>
        ///     Calculate and apply the horizontal component of the size to the RectTransform
        /// </summary>
        public virtual void SetLayoutHorizontal ()
        {
            m_Tracker.Clear ();
            if (m_MinWidth >= 0) HandleSelfFittingAlongAxis (0);
        }

        /// <summary>
        ///     Calculate and apply the vertical component of the size to the RectTransform
        /// </summary>
        public virtual void SetLayoutVertical ()
        {
            if (m_MinHeight >= 0) HandleSelfFittingAlongAxis (1);
        }

        private void HandleSelfFittingAlongAxis (int axis)
        {
            m_Tracker.Add (this, rectTransform, axis == 0 ? DrivenTransformProperties.SizeDeltaX : DrivenTransformProperties.SizeDeltaY);

            if (axis == 0)
                rectTransform.SetSizeWithCurrentAnchors ((RectTransform.Axis) axis, m_MinWidth * Mathf.CeilToInt (Mathf.Sqrt (rectTransform.childCount)));
            else
                rectTransform.SetSizeWithCurrentAnchors ((RectTransform.Axis) axis, m_MinHeight * Mathf.RoundToInt (Mathf.Sqrt (rectTransform.childCount)));
        }

        protected void SetDirty ()
        {
            if (!IsActive ())
                return;

            LayoutRebuilder.MarkLayoutForRebuild (rectTransform);
        }
    }
}