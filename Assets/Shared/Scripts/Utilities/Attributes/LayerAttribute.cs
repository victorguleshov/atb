﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using Utilities;

#endif

namespace Utilities
{
    public class LayerAttribute : PropertyAttribute { }
}

#if UNITY_EDITOR
namespace Internal
{
    [CustomPropertyDrawer (typeof (LayerAttribute))]
    public class LayerAttributeDrawer : PropertyDrawer
    {
        private bool _checked;

        public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.Integer)
            {
                if (!_checked) Warning (property);
                EditorGUI.PropertyField (position, property, label);
                return;
            }

            property.intValue = EditorGUI.LayerField (position, label, property.intValue);
        }

        private void Warning (SerializedProperty property)
        {
            Debug.LogWarning ($"Property <color=brown>{property.name}</color> in object <color=brown>{property.serializedObject.targetObject}</color> is of wrong type. Expected: Int");
            _checked = true;
        }
    }
}
#endif