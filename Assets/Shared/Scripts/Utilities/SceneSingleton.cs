using UnityEngine;

public abstract class SceneSingleton<A> : MonoBehaviour where A : SceneSingleton<A>
{
    private static A _instance;

    [Header ("Scene Singleton")]
    [SerializeField] private bool isPersistant;

    public static A Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<A> ();

                if (_instance == null)
                {
                    _instance = new GameObject (typeof (A).Name).AddComponent<A> ();

                    DontDestroyOnLoad (_instance.gameObject);
                    _instance.isPersistant = true;
                }
            }

            return _instance;
        }
    }

    protected virtual void Awake ()
    {
        if (_instance != null)
        {
            if (_instance == this)
                isPersistant = true;
            else
                Destroy (gameObject);
        }
        else
        {
            _instance = this as A;

            if (isPersistant) DontDestroyOnLoad (gameObject);
        }
    }
}