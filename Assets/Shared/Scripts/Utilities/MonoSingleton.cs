﻿using UnityEngine;

public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    public bool DestroyOnLoad = true;

    public static T Instance { get; private set; }

    #region Unity methods

    protected virtual void Awake ()
    {
        if (!DestroyOnLoad)
        {
            if (!Instance)
                Instance = this as T;
            else
                Destroy (gameObject);
            DontDestroyOnLoad (gameObject);
        }
        else
        {
            Instance = this as T;
        }
    }

    #endregion
}