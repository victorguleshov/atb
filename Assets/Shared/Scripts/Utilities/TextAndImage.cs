using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utilities
{
    public class TextAndImage : MonoBehaviour
    {
        public Button[] Buttons;
        public Image[] Images;
        public TextMeshProUGUI[] Texts;

        public string text
        {
            get => Texts?[0].text ?? string.Empty;
            set
            {
                if (Texts.Length > 0) Texts[0].text = value;
            }
        }

        public Sprite sprite
        {
            get => Images?[0].sprite;
            set
            {
                if (Images.Length > 0) Images[0].sprite = value;
            }
        }

        public Button.ButtonClickedEvent onClick => Buttons?[0].onClick;

        private void Reset ()
        {
            Texts = GetComponentsInChildren<TextMeshProUGUI> (true);
            Images = GetComponentsInChildren<Image> (true);
            Buttons = GetComponentsInChildren<Button> (true);
        }
    }
}