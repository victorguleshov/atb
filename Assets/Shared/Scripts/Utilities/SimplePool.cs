using System;
using System.Collections.Generic;

namespace Utilities
{
    public class SimplePool<T>
    {
        private readonly Dictionary<Type, Stack<T>> dictionary;

        public SimplePool (IEnumerable<T> collection, Func<Type, T> createNewFunc = null)
        {
            dictionary = new Dictionary<Type, Stack<T>> ();
            this.createNewFunc = createNewFunc;
            AddRange (collection);
        }

        public SimplePool (Func<Type, T> createNewFunc = null)
        {
            dictionary = new Dictionary<Type, Stack<T>> ();
            this.createNewFunc = createNewFunc;
        }

        private event Func<Type, T> createNewFunc;

        public void AddRange (IEnumerable<T> collection)
        {
            foreach (var item in collection)
            {
                var type = item.GetType ();
                if (!dictionary.ContainsKey (type)) dictionary.Add (type, new Stack<T> ());
                dictionary[type].Push (item);
            }
        }

        public X Get<X> () where X : T
        {
            var type = typeof (X);
            if (dictionary.ContainsKey (type) && dictionary[type].Count > 0)
                return (X) dictionary[type].Pop ();
            else
                return createNewFunc != null ? (X) createNewFunc.Invoke (type) : default;
        }

        public T Get ()
        {
            foreach (var item in dictionary)
                if (item.Value.Count > 0)
                    return item.Value.Pop ();
            return createNewFunc != null ? createNewFunc.Invoke (typeof (T)) : default;
        }
    }
}