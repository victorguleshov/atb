#if UNITY_EDITOR

using System.Linq;
using UnityEditor;

namespace Internal
{
    public class ReserializeAssetsFeature
    {
        // [MenuItem ("Assets/Reserialize All", false, 40)]
        // private static void ReserializeAllAssets ()
        // {
        //     var pathes = AssetDatabase.GetAllAssetPaths ().ToList ();
        //     AssetDatabase.ForceReserializeAssets (pathes, ForceReserializeAssetsOptions.ReserializeAssetsAndMetadata);
        // }

        [MenuItem ("Assets/Reserialize Selected", false, 40)]
        public static void ReserializeSelectedAssets ()
        {
            var pathes = Selection.objects.Select (AssetDatabase.GetAssetPath).ToList ();
            AssetDatabase.ForceReserializeAssets (pathes, ForceReserializeAssetsOptions.ReserializeAssetsAndMetadata);
        }

        [MenuItem ("Assets/Reserialize Selected", true)]
        private static bool ValidateReserializeSelectedAssets ()
        {
            return EditorUtility.IsPersistent (Selection.activeGameObject);
        }
    }
}

#endif