﻿public static class Consts
{
    public static class Keys
    {
        // Какие-то из этих ключей могут быть остаточным, нигде не используемым, мусором;

        public const string kLivesData = "livesData";
        public const string kPlayerData = "playerData";
        public const string kState = "kState";

        public const string kAFEventFirstLaunch = "kAFEventFirstLaunch";

        public const string kBackground = "Background";
        public const string kLanguage = "Language";

        public const string kNews = "kNews";
        public const string kSessionNumber = "kSessionNumber";
        public const string kBankPurchase = "kBankPurchase";
        public const string kBankPurchaseSum = "kBankPurchaseSum";
        public const string kBonusBoxReceived = "kBonusBoxReceived";
        public const string kEffects = "kEffects";
        public const string kMusic = "kMusic";
        public const string kPiggyBank = "kPiggyBank";
        public const string kPiggyFull = "kPiggyFull";

        public const string kRequestNotification = "kRequestNotificationLastShown";
        public const string kRequestRating = "kRequestRatingLastShown";
        public const string kRequestRatingDone = "RequestStoreReviewDone";
    }

    public static class Rules
    {
        public const float metaGridStepX = 0.72f;
        public const float metaGridStepY = 0.36f;

        public const float coreGridStep = 3.2f;
        public const float coreProjectileRadius = 0.25f;

        public const float coreObjectsDestroyDelay = 0.2f;
        public const float delayForComboBonusesActivation = 0.25f;

        public const int piggyBonusModifier = 8;
    }

    public static class Tags
    {
        public const string FogOfWar = "FogOfWar";
        public const string Projectile = "Projectile";
        public const string BoardBorder = "BoardBorder";
        public const string BoardObject = "BoardObject";
    }

    public static class Layers
    {
        public const string Default = "Default";
        public const string IgnoreRaycast = "Ignore Raycast";
        public const string Player = "Player";
        public const string Environment = "Environment";
        public const string Projectile = "Projectile";
        public const string BoardObject = "BoardObject";

        public const string UI = "UI";
        public const string DeveloperUI = "DeveloperUI";
    }

    public static class SortingLayers
    {
        // Доступные SortingLayers. Записывать сюда новые константы строго в порядке приоритета рендеринга!;

        // Сортировка рендеринга игровых коре-элементов;
        public const string Default = "Default"; // Карта Меты, задник Коре, рендеры игровых объектов Коре;
        public const string DefaultPlus = "DefaultPlus"; // Искажающие пространство эффекты на партиклах Коре (GlassBowl, ExplosiveLight, ReinforcedLight и др.);
        public const string BoardObjectsUI = "BoardObjectsUI"; // ХП бары объектов Коре и рендеры самих объектов в момент перемещения (прыжков) по доске;
        public const string BoardObjectsFX = "BoardObjectsFX"; // FX объектов Коре;

        // Сортировка рендеринга UI элементов во всех сценах;
        public const string GameSceneUI = "GameSceneUI"; // Основной UI в Коре;

        // Имитация <Overlay> пространства для <Screen space - Camera> канвасов;
        public const string OverlayUI = "OverlayUI"; // Используется для Widgets, CutScenes;
        public const string OverlayFX = "OverlayFX"; // Используется для элементов Aiming'a в кат-сценах коре туториала;
    }

    public static class Bundles
    {
        private const string CUTSCENES = "cutscenes";
        public const string PLAYER = "player";
        public const string BACKGROUNDS = "backgrounds";
        public const string SUPPORT = "support";
        public const string SPECIALOFFERS = "specialoffers";

        public static string LEVELS { get; } = "levels";

        public static string[] All => new[] { CUTSCENES, PLAYER, BACKGROUNDS, SUPPORT, SPECIALOFFERS };
        public static string[] ForCore => new[] { CUTSCENES, PLAYER };
        public static string[] ForMeta => new[] { CUTSCENES, PLAYER, BACKGROUNDS, SUPPORT, SPECIALOFFERS };
    }

    public static class Scenes
    {
        public const string StartScene = "StartScene";
        public const string MetaGame = "MetaGame";
        public const string StairsGame = "StairsGame";
        public const string CoreGame = "CoreGame";
        public const string ReloadScene = "ReloadHelper";
    }
}